﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
using VkVideo.VkModels;

namespace VkVideo.VkRepositories
{
  public interface IVkApiProvider
  {
    Task<VKBackendResult<VkAlbumWithPhoto>> AddAlbum(string name, string privacy);

    Task<VKBackendResult<VKList<int>>> AddVideoToAlbum(VkVideoWithFile video, params string[] albumsId);

    Task<VKBackendResult<int>> CreateComment(VkVideoWithFile video, string message);

    Task<VKBackendResult<int>> DeleteAlbum(string albumId);

    Task<VKBackendResult<int>> DeleteUserVideo(VkVideoWithFile video);

    Task<VKBackendResult<VKList<int>>> DeleteVideoFromAlbum(VkVideoWithFile video, params string[] albumsId);

    Task<List<string>> GetAlbumsByVideo(VkVideoWithFile video);

    Task<List<VkCommentWithProfile>> LoadComments(VkVideoWithFile video);

    Task<VKUser> LoadUser();

    Task<List<VkAlbumWithPhoto>> LoadUserAlbums(string userId);

    Task<List<VkVideoWithFile>> VideoGet(string albumId = null);

    Task<VkCatalogs> GetCatalogs(string next = null);

    Task<VkCatalog> GetCatalogItems(string next, string catalogId);

    Task<VkVideoWithFile> GetVideoById(string videoId, string owner);

    Task<VKBackendResult<int>> EditVideo(VkVideoWithFile video, string title, string description);

    Task<VKBackendResult<int>> EditAlbum(string albumId, string name, string privacy);

    Task<List<VkVideoWithFile>> SearchVideo(string q, bool searchOwner = false);

    Task<VKBackendResult<VKList<VkVideoWithFile>>> VideoGetByOffset(string offset, string count);

    Task<VkAlbumWithPhoto> GetAlbumById(string albumId, string userId);
  }
}