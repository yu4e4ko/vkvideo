﻿using Microsoft.Phone.Shell;
using VkVideo.ApplicationContext;

namespace VkVideo.VkRepositories
{
  public class VkRepositoryResolver
  {
    private static VkRepository instance;

    public static VkRepository Instance
    {
      get
      {
        if (instance == null)
        {
          if (PhoneApplicationService.Current.State.ContainsKey(ApplicationConstants.VkRepositoryId)
            && PhoneApplicationService.Current.State[ApplicationConstants.VkRepositoryId] != null)
          {
            instance = PhoneApplicationService.Current.State[ApplicationConstants.VkRepositoryId] as VkRepository;

            if (instance == null)
            {
              throw new System.Exception("VkRepository is null after restore state");
            }
          }
          else
          {
            instance = new VkRepository(new VkApiProvider());
          }
        }
        else
        {
          PhoneApplicationService.Current.State[ApplicationConstants.VkRepositoryId] = instance;
        }

        return instance;
      }
      set
      {
        instance = value;
      }
    }
  }
}
