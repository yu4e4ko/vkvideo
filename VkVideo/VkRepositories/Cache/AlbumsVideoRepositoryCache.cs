﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using VkVideo.VkModels;

namespace VkVideo.VkRepositories.Cache
{
  [DataContract]
  [KnownType(typeof(Dictionary<string, List<VkVideoWithFile>>))]
  public sealed class AlbumsVideoRepositoryCache : IVideoVkRepositoryCache
  {
    [DataMember]
    private Dictionary<string, List<VkVideoWithFile>> _cachedAlbumsVideo;

    public AlbumsVideoRepositoryCache()
    {
      _cachedAlbumsVideo = new Dictionary<string, List<VkVideoWithFile>>();
    }

    public void AddInCache(List<VkVideoWithFile> item, object additionalData = null)
    {
      var albumId = additionalData as string;

      if (string.IsNullOrEmpty(albumId))
      {
        return;
      }

      if (_cachedAlbumsVideo == null)
      {
        _cachedAlbumsVideo = new Dictionary<string, List<VkVideoWithFile>>();
      }

      _cachedAlbumsVideo.Add(albumId, item);
    }

    public void AddVideoInCahce(VkVideoWithFile video, string albumId)
    {
      if (_cachedAlbumsVideo == null)
      {
        _cachedAlbumsVideo = new Dictionary<string, List<VkVideoWithFile>>();
      }

      if (_cachedAlbumsVideo.ContainsKey(albumId))
      {
        _cachedAlbumsVideo[albumId].Insert(0, video);
      }
    }

    public List<VkVideoWithFile> GetFromCahce(object additionalData = null)
    {
      var albumId = additionalData as string;

      if (string.IsNullOrEmpty(albumId) == false && _cachedAlbumsVideo != null && _cachedAlbumsVideo.ContainsKey(albumId))
      {
        return _cachedAlbumsVideo[albumId];
      }

      throw new Exception("AlbumsVideo cache is NULL");
    }

    public bool IsCached(object additionalData = null)
    {
      if (_cachedAlbumsVideo == null)
      {
        _cachedAlbumsVideo = new Dictionary<string, List<VkVideoWithFile>>();
      }

      var albumId = additionalData as string;

      return string.IsNullOrEmpty(albumId) == false && _cachedAlbumsVideo.ContainsKey(albumId);
    }

    public void RemoveVideoFromCache(VkVideoWithFile video, string albumId)
    {
      if (_cachedAlbumsVideo == null)
      {
        _cachedAlbumsVideo = new Dictionary<string, List<VkVideoWithFile>>();
      }

      if (_cachedAlbumsVideo.ContainsKey(albumId))
      {
        var albumVideos = _cachedAlbumsVideo[albumId];
        var videoFromAlbum = albumVideos.FirstOrDefault(r => r.id == video.id);

        if (videoFromAlbum != null)
        {
          albumVideos.Remove(videoFromAlbum);
        }
      }
    }

    public void ResetCache(object additionalData = null)
    {
      var albumId = additionalData as string;

      if (string.IsNullOrEmpty(albumId))
      {
        _cachedAlbumsVideo = new Dictionary<string, List<VkVideoWithFile>>();
      }
      else
      {
        if (_cachedAlbumsVideo.ContainsKey(albumId))
        {
          _cachedAlbumsVideo.Remove(albumId);
        }
      }
    }
  }
}
