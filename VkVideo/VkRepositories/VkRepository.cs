﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows;
using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
using VkVideo.VkModels;
using VkVideo.VkRepositories.Cache;

namespace VkVideo.VkRepositories
{
  [DataContract]
  [KnownType(typeof(VkApiProvider))]
  [KnownType(typeof(UserVkRepositoryCache<VKUser>))]
  [KnownType(typeof(UserAlbumsVkRepositoryCache))]
  [KnownType(typeof(SearchVideoVkRepositoryCache<List<VkVideoWithFile>>))]
  [KnownType(typeof(CommentsVkRepositoryCache<List<VkCommentWithProfile>>))]
  [KnownType(typeof(CatalogsVkRepositoryCache<VkCatalogs>))]
  [KnownType(typeof(AlbumsVideoRepositoryCache))]
  [KnownType(typeof(AlbumsByVideoVkRepositoryCache<List<string>>))]
  [KnownType(typeof(VkCatalog))]
  [KnownType(typeof(VkVideoWithFile))]
  [KnownType(typeof(VkAlbumWithPhoto))]
  public class VkRepository
  {
    [DataMember]
    private readonly IVkApiProvider _apiProvider;

    [DataMember]
    private readonly IVkRepositoryCache<VKUser> _userCache;

    [DataMember]
    private readonly IVkRepositoryCache<List<VkCommentWithProfile>> _commentsCache;

    [DataMember]
    private readonly IVideoVkRepositoryCache _albumVideoCache;

    [DataMember]
    private readonly IVkRepositoryCache<List<string>> _albumsByVideoCache;

    [DataMember]
    private readonly IAlbumVkRepositoryCache _userAlbumsCache;

    [DataMember]
    private readonly IVkRepositoryCache<VkCatalogs> _catalogsCache;

    [DataMember]
    private readonly IVkRepositoryCache<List<VkVideoWithFile>> _searchVideoCache;

    public VkRepository(IVkApiProvider apiProvider)
    {
      _apiProvider = apiProvider;

      _userCache = new UserVkRepositoryCache<VKUser>();
      _commentsCache = new CommentsVkRepositoryCache<List<VkCommentWithProfile>>();
      _albumVideoCache = new AlbumsVideoRepositoryCache();
      _albumsByVideoCache = new AlbumsByVideoVkRepositoryCache<List<string>>();
      _userAlbumsCache = new UserAlbumsVkRepositoryCache();
      _catalogsCache = new CatalogsVkRepositoryCache<VkCatalogs>();
      _searchVideoCache = new SearchVideoVkRepositoryCache<List<VkVideoWithFile>>();
    }

    [DataMember]
    public VkCatalog CurrectCatalog { get; set; }

    [DataMember]
    public VkVideoWithFile CurrentVideo { get; set; }

    [DataMember]
    public VkAlbumWithPhoto CurrentAlbum { get; set; }

    public void ResetCash()
    {
      _userCache.ResetCache();
      _commentsCache.ResetCache();
      _albumVideoCache.ResetCache();
      _albumsByVideoCache.ResetCache();
      _userAlbumsCache.ResetCache();
      _catalogsCache.ResetCache();
      _searchVideoCache.ResetCache();
    }

    public async Task<VKUser> GetCurrentUser()
    {
      if (_userCache.IsCached() == false)
      {
        _userCache.AddInCache(await _apiProvider.LoadUser());
      }

      return _userCache.GetFromCahce();
    }

    public async Task<List<VkAlbumWithPhoto>> GetCurrentUserAlbums()
    {
      if (_userAlbumsCache.IsCached() == false)
      {
        var user = await GetCurrentUser();
        _userAlbumsCache.AddInCache(await _apiProvider.LoadUserAlbums(user.id.ToString()));
      }

      return _userAlbumsCache.GetFromCahce();
    }

    public async Task<VkCatalogs> GetCatalogs(string next = null)
    {
      if (_catalogsCache.IsCached() == false)
      {
        _catalogsCache.AddInCache(await _apiProvider.GetCatalogs());
      }

      if (string.IsNullOrEmpty(next) == false)
      {
        var nextCatalogs = await _apiProvider.GetCatalogs(next);
        _catalogsCache.GetFromCahce().Items.AddRange(nextCatalogs.Items);
        _catalogsCache.GetFromCahce().Next = nextCatalogs.Next;
      }

      return _catalogsCache.GetFromCahce();
    }

    public async Task<VkCatalog> GetCatalogItems(string next, string catalogId)
    {
      var catalog = await _apiProvider.GetCatalogItems(next, catalogId);

      if (_catalogsCache.IsCached())
      {
        var cachedCatalog = _catalogsCache.GetFromCahce().Items.FirstOrDefault(r => r.id == catalogId);

        if (cachedCatalog != null)
        {
          cachedCatalog.Items.AddRange(catalog.Items);
          cachedCatalog.Next = catalog.Next;
        }
      }

      return catalog;
    }

    public async Task<List<VkVideoWithFile>> GetAlbumVideoList(string albumId)
    {
      if (_albumVideoCache.IsCached(albumId) == false)
      {
        _albumVideoCache.AddInCache(await _apiProvider.VideoGet(albumId), albumId);
      }

      return _albumVideoCache.GetFromCahce(albumId);
    }

    public async Task<List<VkCommentWithProfile>> LoadVideoComments(VkVideoWithFile video)
    {
      if (_commentsCache.IsCached(video) == false)
      {
        _commentsCache.AddInCache(await _apiProvider.LoadComments(video), video);
      }

      return _commentsCache.GetFromCahce();
    }

    public async Task CreateComment(VkVideoWithFile video, string message)
    {
      var response = await _apiProvider.CreateComment(video, message);

      if (response.ResultCode == VKResultCode.Succeeded)
      {
        _commentsCache.ResetCache();
      }
    }

    public async Task AddAlbum(string name, string privacy)
    {
      var response = await _apiProvider.AddAlbum(name, privacy);

      if (response.ResultCode == VKResultCode.Succeeded)
      {
        var albumById = await GetAlbumById(response.Data.album_id);
        _userAlbumsCache.AddAlbumInCahce(albumById);
      }
    }

    public async Task DeleteAlbum(string albumId)
    {
      var response = await _apiProvider.DeleteAlbum(albumId);

      if (response.ResultCode == VKResultCode.Succeeded)
      {
        _userAlbumsCache.RemoveAlbumFromCacheById(albumId);
        _albumsByVideoCache.ResetCache();
      }
    }

    public async Task DeleteVideoFromAllAlbums(VkVideoWithFile video)
    {
      var user = await GetCurrentUser();

      var albumsId = await GetAlbumsByVideo(video);
      await DeleteVideoFromAlbum(video, albumsId.ToArray());

      if (video.owner_id == user.id)
      {
        await _apiProvider.DeleteUserVideo(video);
      }
    }

    public async Task DeleteVideoFromAlbum(VkVideoWithFile video, params string[] albumsId)
    {
      var response = await _apiProvider.DeleteVideoFromAlbum(video, albumsId);

      if (response.ResultCode == VKResultCode.Succeeded)
      {
        await UpdateVideoAlbumsCache(video, albumsId, false);
      }
    }

    public async Task<List<string>> GetAlbumsByVideo(VkVideoWithFile video)
    {
      if (_albumsByVideoCache.IsCached(video.id.ToString()) == false)
      {
        _albumsByVideoCache.AddInCache(await _apiProvider.GetAlbumsByVideo(video), video.id.ToString());
      }

      return _albumsByVideoCache.GetFromCahce();
    }

    public async Task AddVideoToAlbum(VkVideoWithFile video, params string[] albumsId)
    {
      var response = await _apiProvider.AddVideoToAlbum(video, albumsId);

      if (response.ResultCode == VKResultCode.Succeeded)
      {
        await UpdateVideoAlbumsCache(video, albumsId, true);
      }
    }

    public async Task<VkVideoWithFile> GetVideoById(string videoId, string owner)
    {
      return await _apiProvider.GetVideoById(videoId, owner);
    }

    public async Task<int> EditVideo(VkVideoWithFile video, string title, string description)
    {
      var response = await _apiProvider.EditVideo(video, title, description);
      return response.Data;
    }

    public async Task EditAlbum(string albumId, string name, string privacy)
    {
      var response = await _apiProvider.EditAlbum(albumId, name, privacy);

      if (response.ResultCode == VKResultCode.Succeeded)
      {
        await LoadAndReplaceAlbumInCache(albumId);
      }
    }

    public async Task<List<VkVideoWithFile>> SeachVideo(string q)
    {
      if (_searchVideoCache.IsCached(q) == false)
      {
        _searchVideoCache.AddInCache(await _apiProvider.SearchVideo(q), q);
      }

      return _searchVideoCache.GetFromCahce();
    }

    public async Task<VkAlbumWithPhoto> GetAlbumById(string albumId)
    {
      var album = await _apiProvider.GetAlbumById(albumId, _userCache.GetFromCahce().id.ToString());
      return album;
    }

    private async Task LoadAndReplaceAlbumInCache(string albumId)
    {
      var albumById = await GetAlbumById(albumId);
      _userAlbumsCache.ReplaceAlbumInCache(albumById, albumId);
    }

    private async Task UpdateVideoAlbumsCache(VkVideoWithFile video, string[] albumsId, bool isAddMode)
    {
      _albumsByVideoCache.ResetCache();

      var shouldReload = false;

      if (_userAlbumsCache.ShouldReloadAllAbums(albumsId.Count()))
      {
        _userAlbumsCache.ResetCache();
        shouldReload = true;
      }

      foreach (var albumId in albumsId)
      {
        if (isAddMode)
        {
          _albumVideoCache.AddVideoInCahce(video, albumId);
        }
        else
        {
          _albumVideoCache.RemoveVideoFromCache(video, albumId);
        }

        if (shouldReload == false)
        {
          await LoadAndReplaceAlbumInCache(albumId);
        }
      }
    }
  }
}
