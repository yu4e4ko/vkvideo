﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using VkVideo.Services;
using VkVideo.ViewModels;

namespace VkVideo.UserControls
{
  public partial class LoadingIcon : UserControl
  {
    private LoadingIconViewModel _viewModel;

    public LoadingIcon()
    {
      InitializeComponent();

      _viewModel = LoadingIconResolver.Instance;
      this.DataContext = _viewModel;

      var binding = new Binding();
      binding.Path = new PropertyPath("LoadingVisibility");
      binding.Mode = BindingMode.TwoWay;
      binding.Source = LoadingIconResolver.Instance;

      LoadingGrid.SetBinding(Grid.VisibilityProperty, binding);
    }
  }
}
