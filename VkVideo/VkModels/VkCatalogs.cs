﻿using System.Collections.Generic;

namespace VkVideo.VkModels
{
  public class VkCatalogs
  {
    public VkCatalogs()
    {
      Items = new List<VkCatalog>();
    }

    public List<VkCatalog> Items { get; set; }

    public string Next { get; set; }
  }
}
