﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using System.Windows.Media;
using System.Linq;
using VkVideo.ApplicationContext;

namespace VkVideo.UserControls
{
  public partial class VideoListControl : UserControl
  {
    public EventHandler<System.Windows.Input.GestureEventArgs> ItemTapHanlder;

    private readonly ContextMenu _contextMenu;

    public VideoListControl()
    {
      InitializeComponent();
      _contextMenu = new ContextMenu();
      MenuItems = new List<Control>();
    }

    public List<Control> MenuItems { get; private set; }

    public void ScrollUp(object sender, EventArgs e)
    {
      Dispatcher.BeginInvoke(() =>
      {
        if (MainList.Items != null && MainList.Items.Count > 0)
        {
          MainList.ScrollIntoView(MainList.Items[0]);
        }
      });
    }

    private void Item_Tap(object sender, System.Windows.Input.GestureEventArgs e)
    {
      var item = MainList.SelectedItem;

      if (ItemTapHanlder != null)
      {
        ItemTapHanlder(item, e);
      }
    }

    // Note: manualy build menu
    private void ItemGrid_Hold(object sender, System.Windows.Input.GestureEventArgs e)
    {
      if (MenuItems.Any() == false)
      {
        return;
      }

      _contextMenu.DataContext = (sender as Grid).DataContext;
      if (_contextMenu.Items.Count == 0)
      {
        foreach(var item in MenuItems)
        {
          _contextMenu.Items.Add(item);
        }
      }

      _contextMenu.VerticalOffset = CalculateVerticalOffset(sender as Grid, MenuItems.Count);

      _contextMenu.IsOpen = true;
      e.Handled = true;
    }

    private double CalculateVerticalOffset(Grid grid, int itemsInMenu)
    {
      var generalTransform = grid.TransformToVisual(null);
      var p = generalTransform.Transform(new Point(0, 0));

      var verticalOffset = p.Y;

      var displayHeight = App.Current.Host.Content.ActualHeight;

      if (displayHeight - verticalOffset < ApplicationConstants.MenuItemHeight * itemsInMenu)
      {
        verticalOffset -= ApplicationConstants.MenuItemHeight * itemsInMenu;
      }

      return verticalOffset;
    }
  }
}
