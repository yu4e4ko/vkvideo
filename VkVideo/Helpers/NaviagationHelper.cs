﻿using Microsoft.Phone.Controls;
using System;
using System.Windows;

namespace VkVideo.Helpers
{
  public class NaviagationHelper
  {
    public static void Navigate(string address)
    {
      try
      {
        var uri = new Uri(address, UriKind.Relative);
        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(uri);
      }
      catch(Exception ex)
      {
        throw new Exception(string.Format("Navigate error. Address: \"{0}\"{1}{2}", address, Environment.NewLine, ex.Message), ex);
      }
    }

    public static void GoBack()
    {
      (Application.Current.RootVisual as PhoneApplicationFrame).GoBack();
    }
  }
}
