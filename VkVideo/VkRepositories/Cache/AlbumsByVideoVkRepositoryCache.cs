﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VkVideo.VkRepositories.Cache
{
  [DataContract]
  [KnownType(typeof(List<string>))]
  public sealed class AlbumsByVideoVkRepositoryCache<T> : IVkRepositoryCache<List<string>>
  {
    [DataMember]
    private List<string> _cachedAlbumsByVideo;
    private string _cachedAlbumsByVideoId;

    public void AddInCache(List<string> item, object additionalData = null)
    {
      var videoId = additionalData as string;

      if (string.IsNullOrEmpty(videoId))
      {
        return;
      }

      _cachedAlbumsByVideo = item;
      _cachedAlbumsByVideoId = videoId;
    }

    public List<string> GetFromCahce(object additionalData = null)
    {
      if (_cachedAlbumsByVideo == null)
      {
        throw new Exception("AlbumsByVideo cache is NULL");
      }

      return _cachedAlbumsByVideo;
    }

    public bool IsCached(object additionalData = null)
    {
      var videoId = additionalData as string;

      if (string.IsNullOrEmpty(videoId))
      {
        return false;
      }

      return _cachedAlbumsByVideoId == videoId && _cachedAlbumsByVideo != null;
    }

    public void ResetCache(object additionalData = null)
    {
      _cachedAlbumsByVideoId = null;
      _cachedAlbumsByVideo = null;
    }
  }
}
