﻿namespace VkVideo.ViewModels
{
  public class SearchControlViewModel : BaseViewModel
  {
    private string _searchText;

    public SearchControlViewModel()
    {

    }

    public string SearchText
    {
      get
      {
        return _searchText;
      }
      set
      {
        _searchText = value;
        OnPropertyChanged("SearchText");
      }
    }
  }
}
