﻿using System;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using VkVideo.ViewModels;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Shell;
using VkVideo.Resources;
using VkVideo.ApplicationContext;
using System.Threading.Tasks;
using VkVideo.Handlers;
using System.Windows.Data;
using System.Collections.Generic;
using VkVideo.VkModels;
using System.Windows.Media;
using System.Linq;

namespace VkVideo.Views
{
  public partial class VideoView : PhoneApplicationPage
  {
    private readonly VideoViewModel _videoViewModel;

    public VideoView()
    {
      InitializeComponent();

      _videoViewModel = new VideoViewModel();
      this.DataContext = _videoViewModel;
      BindRadiobuttonsVisibility();
    }

    public async Task BuildApplicationBar()
    {
      ApplicationBar = new ApplicationBar();

      var addVideoToAlbum = new ApplicationBarIconButton(new Uri(ApplicationConstants.AddMenuIconPath, UriKind.Relative));
      addVideoToAlbum.Text = AppResources.Add_video_to_album_UC;
      addVideoToAlbum.Click += AddVideoToAlbum;
      ApplicationBar.Buttons.Add(addVideoToAlbum);

      if (await _videoViewModel.CanEdit())
      {
        var editTitleMenuItem = new ApplicationBarMenuItem
        {
          Text = AppResources.Edit_title_UC
        };

        editTitleMenuItem.Click += MenuItemEditTitle_Click;
        ApplicationBar.MenuItems.Add(editTitleMenuItem);

        var editDescriptionMenuItem = new ApplicationBarMenuItem
        {
          Text = AppResources.Edit_description_UC
        };

        editDescriptionMenuItem.Click += MenuItemEditDescription_Click;
        ApplicationBar.MenuItems.Add(editDescriptionMenuItem);
      }

      if (await _videoViewModel.CommentsAllowed())
      {
        var addCommentMenuItem = new ApplicationBarMenuItem
        {
          Text = AppResources.Add_comment_UC
        };

        addCommentMenuItem.Click += MenuAddComment_Click;
        ApplicationBar.MenuItems.Add(addCommentMenuItem);
      }
    }

    private async void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
    {
      await _videoViewModel.Init();
      await BuildApplicationBar();

      // Note: ItemsControl uses big size of memory. I use grid for best performance
      var comments = _videoViewModel.Comments;
      BuildCommentsGrid(comments);
    }

    private void AddVideoToAlbum(object sender, EventArgs e)
    {
      _videoViewModel.AddVideoToAlbum(_videoViewModel.Video);
    }

    private void BindRadiobuttonsVisibility()
    {
      var binding = new Binding
      {
        Mode = BindingMode.TwoWay,
        Source = _videoViewModel,
        Path = new PropertyPath("ShowRadiobuttons")
      };

      GridRadioButtons.SetBinding(Grid.VisibilityProperty, binding);
    }

    private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      // Note: hack for selectable = false
      if (null != sender && sender is ListBox)
      {
        var listBox = sender as ListBox;
        listBox.SelectedIndex = -1;
      }
    }

    private void RadioBtn_Checked(object sender, RoutedEventArgs e)
    {
      var radioButton = sender as RadioButton;
      var content = radioButton.Content.ToString();
      _videoViewModel.RadioButtonChecked(content);
    }

    private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
    {
      _videoViewModel.ShowVideo();
    }

    private void Image_ImageFailed(object sender, ExceptionRoutedEventArgs e)
    {
      ImageVideo.Source = new BitmapImage(new Uri(ApplicationConstants.EmptyImagePath, UriKind.Relative));
    }

    private async void MenuAddComment_Click(object sender, EventArgs e)
    {
      if (await _videoViewModel.AddComment())
      {
        var comments = _videoViewModel.Comments;

        if (comments.Count < ApplicationConstants.MaxCommentsForVideo)
        {
          AddCommentInGrid(comments);
        }
        else
        {
          BuildCommentsGrid(_videoViewModel.Comments);
        }
      }
    }

    private async void MenuItemEditTitle_Click(object sender, EventArgs e)
    {
      await _videoViewModel.EditTitle();
    }

    private async void MenuItemEditDescription_Click(object sender, EventArgs e)
    {
      await _videoViewModel.EditDescription();
    }

    private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      VkVideoEventHandler.HandleBackKeyPress(sender, e);
    }

    private void AddCommentInGrid(List<VkCommentWithProfile> comments)
    {
      if (comments == null || comments.Any() == false)
      {
        return;
      }

      var comment = comments.Last();

      GridComments.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0, GridUnitType.Auto) });

      var commentRow = BuildCommentRow(comment);
      Grid.SetRow(commentRow, comments.Count - 1);
      GridComments.Children.Add(commentRow);
    }

    private void BuildCommentsGrid(List<VkCommentWithProfile> comments)
    {
      GridComments.Children.Clear();

      var count = 0;
      foreach(var comment in comments)
      {
        GridComments.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0, GridUnitType.Auto) });

        var commentRow = BuildCommentRow(comment);
        Grid.SetRow(commentRow, count);
        GridComments.Children.Add(commentRow);

        count++;
      }
    }

    private Grid BuildCommentRow(VkCommentWithProfile comment)
    {
      var grid = new Grid
      {
        Margin = new Thickness(10, 10, 0, 10)
      };

      grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0, GridUnitType.Auto) });
      grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

      var ellipseSize = 25;
      var ellipseGeometry = new EllipseGeometry
      {
        Center = new Point(ellipseSize, ellipseSize),
        RadiusX = ellipseSize,
        RadiusY = ellipseSize
      };

      var image = new Image
      {
        Source = new BitmapImage(new Uri(comment.Photo_100)),
        VerticalAlignment = VerticalAlignment.Top,
        Width = ellipseSize * 2,
        Clip = ellipseGeometry
      };

      var contentGrid = BuildCommentTextGrid(comment);

      Grid.SetColumn(image, 0);
      Grid.SetColumn(contentGrid, 1);

      grid.Children.Add(image);
      grid.Children.Add(contentGrid);

      var background = new SolidColorBrush(Color.FromArgb(0xFF, 0x3D, 0x50, 0x66));
      var gridWrapper = new Grid
      {
        Background = background
      };

      gridWrapper.Children.Add(grid);
      return gridWrapper;
    }

    private Grid BuildCommentTextGrid(VkCommentWithProfile comment)
    {
      var grid = new Grid();
      grid.Margin = new Thickness(10, 0, 10, 0);

      grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0, GridUnitType.Auto) });
      grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

      var nameTextBlock = new TextBlock
      {
        Text = comment.FullName,
        TextWrapping = TextWrapping.Wrap,
        Foreground = StyleResources.AdditionalRecordsBrush
      };

      var commentContentTextBlock = new TextBlock
      {
        Text = comment.FormattedText,
        TextWrapping = TextWrapping.Wrap
      };

      Grid.SetRow(nameTextBlock, 0);
      Grid.SetRow(commentContentTextBlock, 1);

      grid.Children.Add(nameTextBlock);
      grid.Children.Add(commentContentTextBlock);

      return grid;
    }
  }
}
