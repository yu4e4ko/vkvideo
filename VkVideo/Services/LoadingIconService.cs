﻿using VkVideo.ViewModels;
using System;
using System.Windows;
using System.Windows.Threading;

namespace VkVideo.Services
{
  public class LoadingIconService
  {
    private static LoadingIconService _instance;

    private LoadingIconViewModel _loadingViewModel;

    private const int ShowLoadingDelay = 300;
    private const int HideLoadingDelay = 200;

    private DispatcherTimer _timer;
    private EventHandler _tickEventHandler;

    private DispatcherTimer _hideTimer;
    private EventHandler _hideTickEventHandler;

    public LoadingIconService()
    {
      _loadingViewModel = LoadingIconResolver.Instance;
    }

    public static LoadingIconService Instance
    {
      get
      {
        if (_instance == null)
        {
          _instance = new LoadingIconService();
        }

        return _instance;
      }
    }

    public void ShowLoadingDialog()
    {
      if (_hideTimer != null)
      {
        _hideTimer.Stop();
      }

      if (_tickEventHandler != null)
      {
        return;
      }

      _tickEventHandler = new EventHandler(ShowLoading);

      _timer = new DispatcherTimer();
      _timer.Tick += _tickEventHandler;
      _timer.Interval = TimeSpan.FromMilliseconds(ShowLoadingDelay);
      _timer.Start();
    }

    public void HideLoadingDialog()
    {
      if (_hideTimer != null)
      {
        _hideTimer.Start();
        return;
      }

      _hideTickEventHandler = new EventHandler(HideLoading);

      _hideTimer = new DispatcherTimer();
      _hideTimer.Tick += _hideTickEventHandler;
      _hideTimer.Interval = TimeSpan.FromMilliseconds(HideLoadingDelay);
      _hideTimer.Start();
    }

    public void HideLoadingDialogForce()
    {
      HideLoading(null, null);
    }

    private void HideLoading(object sender, EventArgs args)
    {
      _loadingViewModel.LoadingVisibility = Visibility.Collapsed;
      Dispose();
    }

    private void ShowLoading(object sender, EventArgs args)
    {
      if (_timer != null)
      {
        _timer.Stop();
      }

      _loadingViewModel.LoadingVisibility = Visibility.Visible;
    }

    public void Dispose()
    {
      DisposeTimer();
      DisposeHideTimer();
    }

    private void DisposeTimer()
    {
      if (_timer != null)
      {
        if (_tickEventHandler != null)
        {
          _timer.Tick -= _tickEventHandler;
        }

        _timer.Stop();
        _timer = null;
        _tickEventHandler = null;
      }
    }

    private void DisposeHideTimer()
    {
      if (_hideTimer != null)
      {
        if (_hideTickEventHandler != null)
        {
          _hideTimer.Tick -= _hideTickEventHandler;
        }

        _hideTimer.Stop();
        _hideTimer = null;
        _hideTickEventHandler = null;
      }
    }
  }
}
