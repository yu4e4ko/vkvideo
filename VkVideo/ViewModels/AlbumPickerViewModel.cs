﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VkVideo.VkRepositories;
using VkVideo.Helpers;
using VkVideo.VkModels;
using VkVideo.Resources;
using VkVideo.ApplicationContext;

namespace VkVideo.ViewModels
{
  public class AlbumPickerViewModel : BaseViewModel
  {
    private VkVideoWithFile _currentVideo;
    private List<VkAlbumWithPhoto> _albums;
    private List<string> _albumsContainVideo;

    public AlbumPickerViewModel()
    {
      _albums = new List<VkAlbumWithPhoto>();
      _albumsContainVideo = new List<string>();
    }

    #region properties

    public List<VkAlbumWithPhoto> Albums
    {
      get
      {
        return _albums;
      }
      set
      {
        _albums = value;
        OnPropertyChanged("Albums");
      }
    }

    #endregion

    #region methods

    public async Task Init()
    {
      _currentVideo = VkRepositoryResolver.Instance.CurrentVideo;
      _albumsContainVideo = (await VkRepositoryResolver.Instance.GetAlbumsByVideo(_currentVideo)).ToList();
      await LoadAlbums();
    }

    public async Task LoadAlbums()
    {
      var albums = new List<VkAlbumWithPhoto>
      {
        new VkAlbumWithPhoto { id = ApplicationConstants.MyVideosAlbumId, title = AppResources.My_videos_UC }
      };

      albums.AddRange(await VkRepositoryResolver.Instance.GetCurrentUserAlbums());
     
      var albumsContainsVideo = await VkRepositoryResolver.Instance.GetAlbumsByVideo(_currentVideo);

      foreach (var album in albums)
      {
        album.IsContainsCurrentVideo = albumsContainsVideo.Contains(album.id);
      }

      Albums = albums;
    }

    public async Task SaveChanges()
    {
      await Save();
      NaviagationHelper.GoBack();
    }

    public void RedirectToAddAlbum()
    {
      VkRepositoryResolver.Instance.CurrentAlbum = null;
      NaviagationHelper.Navigate("/Views/AddAlbumView.xaml");
    }

    private async Task Save()
    {
      var shouldBeAdded = new List<string>();
      var shouldBeDeleted = new List<string>();

      foreach(var album in Albums)
      {
        if (_albumsContainVideo.Contains(album.id))
        {
          if (album.IsContainsCurrentVideo == false)
          {
            shouldBeDeleted.Add(album.id);
          }
        }
        else
        {
          if (album.IsContainsCurrentVideo)
          {
            shouldBeAdded.Add(album.id);
          }
        }
      }

      if (shouldBeDeleted.Any())
      {
        await VkRepositoryResolver.Instance.DeleteVideoFromAlbum(_currentVideo, shouldBeDeleted.ToArray());
      }

      if (shouldBeAdded.Any())
      {
        await VkRepositoryResolver.Instance.AddVideoToAlbum(_currentVideo, shouldBeAdded.ToArray());
      }
    }

    #endregion
  }
}
