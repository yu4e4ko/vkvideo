﻿namespace VkVideo.VkModels
{
  public class VkVideoFiles
  {
    public string Flv_320 { get; set; }

    public string External { get; set; }

    public string Mp4_240 { get; set; }

    public string Mp4_360 { get; set; }

    public string Mp4_480 { get; set; }

    public string Mp4_720 { get; set; }

    public VideoFormat GetVideoFormat()
    {
      if (string.IsNullOrEmpty(this.External) == false)
      {
        return VideoFormat.External;
      }
      else if (string.IsNullOrEmpty(this.Flv_320) == false)
      {
        return VideoFormat.Flv;
      }
      else
      {
        if (string.IsNullOrEmpty(this.Mp4_240) == false
          || string.IsNullOrEmpty(this.Mp4_360) == false
          || string.IsNullOrEmpty(this.Mp4_480) == false
          || string.IsNullOrEmpty(this.Mp4_720) == false)
        {
          return VideoFormat.Mp4;
        }
      }

      return VideoFormat.None;
    }
  }

  public enum VideoFormat
  {
    Mp4,

    Flv,

    External,

    None
  }
}
