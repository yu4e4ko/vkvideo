﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
using VkVideo.ApplicationContext;
using VkVideo.Extensions;
using VkVideo.Resources;
using VkVideo.VkModels;

namespace VkVideo.VkRepositories
{
  [DataContract]
  public class VkApiProvider : IVkApiProvider
  {
    public async Task<List<VkCommentWithProfile>> LoadComments(VkVideoWithFile video)
    {
      if (video == null)
      {
        return new List<VkCommentWithProfile>();
      }

      var commentsWithProfiles = new List<VkCommentWithProfile>();
      var commentsPerRequest = ApplicationConstants.MaxCommentsForVideo;

      var request = new VKRequestParameters(
        "video.getComments",
        "video_id", video.id.ToString(),
        "owner_id", video.owner_id.ToString(),
        "count", commentsPerRequest.ToString(),
        "extended", "1",
        "sort", "desc",
        "offset", commentsWithProfiles.Count.ToString());

      var response = await request.GetResponseAsync<VkListVideoComments<VKComment>>("video.getComments");
      var comments = response.Data.items;

      for (var i = 0; i < comments.Count; i++)
      {
        var profile = response.Data.Profiles.FirstOrDefault(r => r.id == comments[i].from_id);

        if (profile == null)
        {
          continue;
        }

        var commentWithProfile = new VkCommentWithProfile(response.Data.items[i]);
        commentWithProfile.ProfileId = profile.id;
        commentWithProfile.Photo_50 = profile.photo_50;
        commentWithProfile.Photo_100 = profile.photo_100;
        commentWithProfile.Photo_200 = profile.photo_200;
        commentWithProfile.FirstName = profile.first_name;
        commentWithProfile.LastName = profile.last_name;

        commentsWithProfiles.Add(commentWithProfile);
      }

      // Note: it's important
      commentsWithProfiles.Reverse();

      if (response.Data.count > commentsPerRequest)
      {
        MessageBox.Show(string.Format(AppResources.Load_limit_comments_UC, commentsPerRequest));
      }

      return commentsWithProfiles;
    }

    public async Task<List<VkVideoWithFile>> VideoGet(string albumId = null)
    {
      var videoList = new List<VkVideoWithFile>();
      var videoCount = int.MaxValue;

      // Note: temp solution
      var loadLimit = 2000;
      while (videoList.Count < videoCount && videoList.Count < loadLimit)
      {
        var request = new VKRequestParameters(
          "video.get",
          "count", "200",
          "extended", "1", 
          "offset", videoList.Count.ToString());

        if (string.IsNullOrEmpty(albumId) == false && albumId != ApplicationConstants.MyVideosAlbumId)
        {
          request.Parameters.Add("album_id", albumId.ToString());
        }

        var response = await request.GetResponseAsync<VKList<VkVideoWithFile>>("video.get");
        videoCount = response.Data.count;
        videoList.AddRange(response.Data.items);

        LargeRequestSleeping(videoList.Count < videoCount);
      }

      if (loadLimit < videoCount)
      {
        MessageBox.Show(string.Format(AppResources.Load_limit_UC, loadLimit));
      }

      return videoList;
    }

    public async Task<VKUser> LoadUser()
    {
      var request = new VKRequestParameters("users.get", "fields", "photo_200, city, country");

      var response = await request.GetResponseAsync<List<VKUser>>("users.get");
      return response.Data[0];
    }

    public async Task<List<VkAlbumWithPhoto>> LoadUserAlbums(string userId)
    {
      var albumList = new List<VkAlbumWithPhoto>();
      var albumsCount = int.MaxValue;

      var loadLimit = 2000;
      while (albumList.Count < albumsCount && albumList.Count < loadLimit)
      {
        var request = new VKRequestParameters(
          "video.getAlbums",
          "owner_id", userId,
          "count", "100",
          "extended", "1",
          "offset", albumList.Count.ToString());

        var response = await request.GetResponseAsync<VKList<VkAlbumWithPhoto>>("video.getAlbums");

        albumsCount = response.Data.count;
        albumList.AddRange(response.Data.items);

        LargeRequestSleeping(albumList.Count < albumsCount);
      }

      if (loadLimit < albumsCount)
      {
        MessageBox.Show(string.Format(AppResources.Load_limit_UC, loadLimit));
      }

      return albumList;
    }

    public async Task<VKBackendResult<int>> DeleteUserVideo(VkVideoWithFile video)
    {
      var request = new VKRequestParameters(
        "video.delete",
        "owner_id", video.owner_id.ToString(),
        "video_id", video.id.ToString());

      return await request.GetResponseAsync<int>("video.delete");
    }

    public async Task<VKBackendResult<int>> CreateComment(VkVideoWithFile video, string message)
    {
      var request = new VKRequestParameters(
          "video.createComment",
          "video_id", video.id.ToString(),
          "owner_id", video.owner_id.ToString(),
          "message", message);

      return await request.GetResponseAsync<int>("video.createComment");
    }

    public async Task<VKBackendResult<VkAlbumWithPhoto>> AddAlbum(string name, string privacy)
    {
      var request = new VKRequestParameters(
        "video.addAlbum",
        "title", name,
        "privacy", privacy);

      return await request.GetResponseAsync<VkAlbumWithPhoto>("video.addAlbum");
    }

    public async Task<VKBackendResult<int>> DeleteAlbum(string albumId)
    {
      var request = new VKRequestParameters(
      "video.deleteAlbum",
      "album_id", albumId);

      return await request.GetResponseAsync<int>("video.deleteAlbum");
    }

    public async Task<VKBackendResult<VKList<int>>> DeleteVideoFromAlbum(VkVideoWithFile video, params string[] albumsId)
    {
      var albumIdLine = albumsId.Aggregate((current, next) => current + ", " + next);

      var request = new VKRequestParameters(
        "video.removeFromAlbum",
        "album_ids", albumIdLine,
        "owner_id", video.owner_id.ToString(),
        "video_id", video.id.ToString());

      return await request.GetResponseAsync<VKList<int>>("video.removeFromAlbum");
    }

    public async Task<List<string>> GetAlbumsByVideo(VkVideoWithFile video)
    {
      var request = new VKRequestParameters(
        "video.getAlbumsByVideo",
        "owner_id", video.owner_id.ToString(),
        "video_id", video.id.ToString());

      var response = await request.GetResponseAsync<List<string>>("video.getAlbumsByVideo");
      return response.Data;
    }

    public async Task<VKBackendResult<VKList<int>>> AddVideoToAlbum(VkVideoWithFile video, params string[] albumsId)
    {
      var albumIdLine = albumsId.Aggregate((current, next) => current + ", " + next);

      var request = new VKRequestParameters(
        "video.addToAlbum",
        "album_ids", albumIdLine,
        "owner_id", video.owner_id.ToString(),
        "video_id", video.id.ToString());

      return await request.GetResponseAsync<VKList<int>>("video.addToAlbum");
    }

    public async Task<VkCatalogs> GetCatalogs(string next = null)
    {
      var request = new VKRequestParameters(
        "video.getCatalog",
        "count", "6",
        "items_count", "10",
        "filters", "other");

      if (string.IsNullOrEmpty(next) == false)
      {
        request.Parameters.Add("from", next);
      }

      var response = await request.
        GetResponseAsync<VkCatalogs>("video.getCatalog");

      // Note: Vk doesn't return can_comment field. Therefore "can_comment" always is false. Fix it.
      response.Data.Items.ForEach(r => r.Items.ForEach(video => video.can_comment = true));

      return response.Data;
    }

    public async Task<VkCatalog> GetCatalogItems(string next, string catalogId)
    {
      var request = new VKRequestParameters(
        "video.getCatalogSection",
        "section_id", catalogId,
        "count", "10",
        "extended", "1",
        "from", next);

      var response = await request.
        GetResponseAsync<VkCatalog>("video.getCatalogSection");

      // Note: Vk doesn't return can_comment field. Therefore "can_comment" always is false. Fix it.
      response.Data.Items.ForEach(r => r.can_comment = true);

      return response.Data;
    }

    public async Task<VkVideoWithFile> GetVideoById(string videoId, string owner)
    {
      var request = new VKRequestParameters(
        "video.get",
        "count", "1",
        "extended", "1",
        "videos", string.Format("{0}_{1}", owner, videoId));

      var response = await request.GetResponseAsync<VKList<VkVideoWithFile>>("video.get");

      return response.Data.items.FirstOrDefault();
    }

    public async Task<VKBackendResult<int>> EditVideo(VkVideoWithFile video, string title, string description)
    {
      var request = new VKRequestParameters(
        "video.edit",
        "owner_id", video.owner_id.ToString(),
        "video_id", video.id.ToString(),
        "name", title,
        "desc", description);

      var response = await request.GetResponseAsync<int> ("video.edit");

      return response;
    }

    public async Task<VKBackendResult<int>> EditAlbum(string albumId, string name, string privacy)
    {
      var request = new VKRequestParameters(
        "video.editAlbum",
        "album_id", albumId,
        "title", name,
        "privacy", privacy);

      return await request.GetResponseAsync<int>("video.editAlbum");
    }

    public async Task<List<VkVideoWithFile>> SearchVideo(string q, bool searchOwner = false)
    {
      var request = new VKRequestParameters(
         "video.search",
         "count", "50",
         "q", q);

      if (searchOwner)
      {
        request.Parameters.Add("search_own", "1");
      }

      var response = await request.GetResponseAsync<VKList<VkVideoWithFile>>("video.search");

      // Note: Vk doesn't return can_comment field. Therefore "can_comment" always is false. Fix it.
      response.Data.items.ForEach(video => video.can_comment = true);
      return response.Data.items;
    }

    public async Task<VKBackendResult<VKList<VkVideoWithFile>>> VideoGetByOffset(string offset, string count)
    {
      var request = new VKRequestParameters(
          "video.get",
          "count", count ?? "100",
          "extended", "1",
          "offset", offset ?? "0");

      var response = await request.GetResponseAsync<VKList<VkVideoWithFile>>("video.get (by offset)");
      return response;
    }

    public async Task<VkAlbumWithPhoto> GetAlbumById(string albumId, string userId)
    {
      var request = new VKRequestParameters(
        "video.getAlbumById",
        "owner_id", userId,
        "album_id", albumId);

      var response = await request.GetResponseAsync<VkAlbumWithPhoto>("video.getAlbumById");
      return response.Data;
    }


    #region helpers

    private void LargeRequestSleeping(bool predicate)
    {
      if (predicate)
      {
        Thread.Sleep(300);
      }
    }

    #endregion
  }
}
