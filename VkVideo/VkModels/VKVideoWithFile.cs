﻿using VK.WindowsPhone.SDK.API.Model;
using VkVideo.Resources;

namespace VkVideo.VkModels
{
  public class VkVideoWithFile : VKVideo
  {
    public VkVideoWithFile()
    {
      this.Files = new VkVideoFiles();
    }

    public bool can_comment { get; set; }

    public bool can_add { get; set; }

    public VkVideoFiles Files { get; set; }

    public string DurationString
    {
      get
      {
        var hours = duration / 3600;
        var minutess = (duration - hours * 3600) / 60;
        var seconds = duration - hours * 3600 - minutess * 60;

        return string.Format("{0:00}:{1:00}:{2:00}", hours, minutess, seconds);
      }
    }

    public string ViewString
    {
      get
      {
        return string.Format("{0}: {1}", AppResources.Views_UC, views);
      }
    }

    public bool ExistFormat_720
    {
      get
      {
        return string.IsNullOrEmpty(Files.Mp4_720) == false;
      }
    }

    public bool ExistFormat_480
    {
      get
      {
        return string.IsNullOrEmpty(Files.Mp4_480) == false;
      }
    }

    public bool ExistFormat_360
    {
      get
      {
        return string.IsNullOrEmpty(Files.Mp4_360) == false;
      }
    }

    public bool ExistFormat_240
    {
      get
      {
        return string.IsNullOrEmpty(Files.Mp4_240) == false;
      }
    }

    public bool ExternalFormat
    {
      get
      {
        return Files.GetVideoFormat() == VideoFormat.Flv || Files.GetVideoFormat() == VideoFormat.External;
      }
    }
  }
}
