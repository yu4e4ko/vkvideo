﻿using System.Windows.Controls;
using VkVideo.Resources;

namespace VkVideo.UserControls
{
  public class CustomButton : Button
  {
    public CustomButton()
    {
      Background = StyleResources.MainForegroundBrush;
      Foreground = StyleResources.MainBackgroundBrush;
      BorderBrush = StyleResources.MainBorderBrush;
    }
  }
}
