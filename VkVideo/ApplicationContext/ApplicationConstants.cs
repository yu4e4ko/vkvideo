﻿namespace VkVideo.ApplicationContext
{
  public class ApplicationConstants
  {
    public const string MyVideosAlbumId = "-2";

    public const string EmptyImagePath = "/Resources/Images/emptyImage.png";

    public const string AddMenuIconPath = "/Resources/Images/ApplicationBar/add.png";

    public const string SaveMenuIconPath = "/Resources/Images/ApplicationBar/save.png";

    public const string CheckMenuIconPath = "/Resources/Images/ApplicationBar/check.png";

    public const string DeleteMenuIconPath = "/Resources/Images/ApplicationBar/delete.png";

    public const int MenuItemHeight = 60;

    // Note: max value = 100
    public const int MaxCommentsForVideo = 100;

    public const string VkRepositoryId = "VkRepo";
  }
}
