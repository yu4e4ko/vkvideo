﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using VkVideo.Helpers;
using VkVideo.VkModels;
using VkVideo.VkRepositories;

namespace VkVideo.ViewModels
{
  public class CatalogViewModel : BaseViewModel
  {
    private const int MaxVideoCount = 89;

    private string _name;
    private ObservableCollection<VkVideoWithFile> _video;
    private string _next;
    private string _sectionId;
    private Visibility _catalogMoreButtonVisibility;
    private bool _moreButtonEnabled;

    public string Name
    {
      get
      {
        return _name;
      }
      set
      {
        _name = value;
        OnPropertyChanged("Name");
      }
    }

    public ObservableCollection<VkVideoWithFile> Video
    {
      get
      {
        return _video;
      }
      set
      {
        _video = value;
        OnPropertyChanged("Video");
      }
    }

    public Visibility CatalogMoreButtonVisibility
    {
      get
      {
        return _catalogMoreButtonVisibility;
      }
      set
      {
        _catalogMoreButtonVisibility = value;
        OnPropertyChanged("CatalogMoreButtonVisibility");
      }
    }

    public bool MoreButtonEnabled
    {
      get
      {
        return _moreButtonEnabled;
      }
      set
      {
        _moreButtonEnabled = value;
        OnPropertyChanged("MoreButtonEnabled");
      }
    }

    public void Init()
    {
      var catalog = VkRepositoryResolver.Instance.CurrectCatalog;
      Name = catalog.Name;
      Video = new ObservableCollection<VkVideoWithFile>(catalog.Items);
      _next = catalog.Next;
      _sectionId = catalog.id;

      MoreButtonEnabled = true;
      SetMoreButtonVisibility();
    }

    public async Task MoreVideo()
    {
      if (Video.Count > MaxVideoCount)
      {
        return;
      }

      MoreButtonEnabled = false;
      var catalog = await VkRepositoryResolver.Instance.GetCatalogItems(_next, _sectionId);
      _next = catalog.Next;

      foreach(var video in catalog.Items)
      {
        Video.Add(video);
      }

      MoreButtonEnabled = true;
      SetMoreButtonVisibility();
    }

    public void OpenVideo(VkVideoWithFile video)
    {
      CommonHelper.ExpectedNotNullObject(video, "Open Video");

      VkRepositoryResolver.Instance.CurrentVideo = video;
      NaviagationHelper.Navigate("/Views/VideoView.xaml");
    }

    private void SetMoreButtonVisibility()
    {
      CatalogMoreButtonVisibility = Video.Count >= MaxVideoCount ? Visibility.Collapsed : Visibility.Visible;
    }
  }
}
