﻿using Microsoft.Phone.Controls;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using VkVideo.ApplicationModels;
using VkVideo.Extensions;
using VkVideo.Helpers;
using VkVideo.Resources;
using VkVideo.VkModels;
using VkVideo.VkRepositories;

namespace VkVideo.ViewModels
{
  public abstract class BaseViewModel : INotifyPropertyChanged, IDisposable
  {
    public event PropertyChangedEventHandler PropertyChanged;

    public void Dispose()
    {
      this.OnDispose();
    }

    protected virtual void OnDispose()
    {
    }

    protected void OnPropertyChanged(string propertyName)
    {
      var handler = this.PropertyChanged;

      if (handler != null)
      {
        var eventArgs = new PropertyChangedEventArgs(propertyName);
        handler(this, eventArgs);
      }
    }

    #region common methods

    public void AddVideoToAlbum(VkVideoWithFile video)
    {
      CommonHelper.ExpectedNotNullObject(video, "Add video in album");

      VkRepositoryResolver.Instance.CurrentVideo = video;
      NaviagationHelper.Navigate("/Views/AlbumPickerView.xaml");
    }

    public async Task<bool> DeleteAlbumBase(VkAlbumWithPhoto album)
    {
      CommonHelper.ExpectedNotNullObject(album, "Delete Album");
      var albumId = album.id;

      var dialogOptions = new CustomDialogOptionsModel
      {
        Message = string.Format("{0}?", AppResources.Delete_album_UC)
      };

      var dialogResult = await this.ShowCustomDialog(dialogOptions);
      if (dialogResult.CustomMessageBoxResult == CustomMessageBoxResult.LeftButton)
      {
        await VkRepositoryResolver.Instance.DeleteAlbum(albumId);
        return true;
      }

      return false;
    }

    #endregion
  }
}
