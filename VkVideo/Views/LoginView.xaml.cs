﻿using System.Windows;
using Microsoft.Phone.Controls;
using VkVideo.ViewModels;
using System;
using VkVideo.Resources;

namespace VkVideo.Views
{
  public partial class LoginView : PhoneApplicationPage
  {
    private LoginViewModel _loginViewModel;
    private EventHandler _switchLanguageHandler;

    public LoginView()
    {
      InitializeComponent();
      _switchLanguageHandler += SwitchLanguage;

      _loginViewModel = new LoginViewModel(_switchLanguageHandler);
      DataContext = _loginViewModel;
    }

    private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      e.Cancel = true;
      App.Current.Terminate();
    }

    private void SwitchLanguage(object sender, EventArgs e)
    {
      BtnLogout.Content = AppResources.Log_out_UC;
      BtnManage.Content = AppResources.My_videos_UC;
      TxtLangOptions.Text = AppResources.Language_options_UC;
    }

    private async void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
    {
      await _loginViewModel.Init();
    }

    private async void BtnManage_Click(object sender, RoutedEventArgs e)
    {
      await _loginViewModel.Manage();
    }

    private async void BtnLogout_Click(object sender, RoutedEventArgs e)
    {
      await _loginViewModel.LogOut();
    }
  }
}