﻿using VK.WindowsPhone.SDK.API.Model;
using VkVideo.Resources;

namespace VkVideo.VkModels
{
  public class VkAlbumWithPhoto : VKAlbum
  {
    public string photo_320 { get; set; }

    public bool IsContainsCurrentVideo { get; set; }

    public VKPrivacy Privacy { get; set; }

    public string album_id { get; set; }

    public int count { get; set; }

    public string CountString
    {
      get
      {
        return string.Format("[{0}: {1}]", AppResources.Records_UC, count);
      }
    }
  }
}
