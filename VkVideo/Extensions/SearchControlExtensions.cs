﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using VkVideo.UserControls;

namespace VkVideo.Extensions
{
  public static class SearchControlExtensions
  {
    public static SearchControl WithBindSearchText(this SearchControl @this, object source, string pathName)
    {
      var binding = new Binding
      {
        Mode = BindingMode.TwoWay,
        Source = source,
        Path = new PropertyPath(pathName)
      };

      @this.TxtSearchText.SetBinding(TextBox.TextProperty, binding);

      return @this;
    }

    public static SearchControl WithSearchButtonEvent(this SearchControl @this, RoutedEventHandler eventHandler)
    {
      @this.BtnSearchControl.Click += eventHandler;
      return @this;
    }
  }
}
