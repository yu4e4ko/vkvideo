﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using VkVideo.ViewModels;
using Microsoft.Phone.Shell;
using VkVideo.Resources;
using VkVideo.ApplicationContext;
using VkVideo.Handlers;

namespace VkVideo.Views
{
  public partial class AlbumPickerView : PhoneApplicationPage
  {
    private readonly AlbumPickerViewModel _albumPickerViewModel;
     
    public AlbumPickerView()
    {
      InitializeComponent();

      _albumPickerViewModel = new AlbumPickerViewModel();
      this.DataContext = _albumPickerViewModel;

      BuildActionBar();
    }

    public void BuildActionBar()
    {
      ApplicationBar = new ApplicationBar();

      // Create a new button and set the text value to the localized string from AppResources.
      ApplicationBarIconButton saveButton = new ApplicationBarIconButton(new Uri(ApplicationConstants.CheckMenuIconPath, UriKind.Relative));
      saveButton.Text = AppResources.Save_UC;
      saveButton.Click += SaveIconButton_Click;
      ApplicationBar.Buttons.Add(saveButton);

      var createAlbumMenuItem = new ApplicationBarMenuItem
      {
        Text = AppResources.Create_new_album_UC
      };

      createAlbumMenuItem.Click += AddAlbumButton_Click;
      ApplicationBar.MenuItems.Add(createAlbumMenuItem);
    }

    private async void SaveIconButton_Click(object sender, EventArgs e)
    {
      await _albumPickerViewModel.SaveChanges();
    }

    private void AddAlbumButton_Click(object sender, EventArgs e)
    {
      _albumPickerViewModel.RedirectToAddAlbum();
    }

    private async void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
    {
      await _albumPickerViewModel.Init();
    }

    private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      VkVideoEventHandler.HandleBackKeyPress(sender, e);
    }
  }
}