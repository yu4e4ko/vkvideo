﻿using System;
using System.Runtime.Serialization;
using VK.WindowsPhone.SDK.API.Model;

namespace VkVideo.VkRepositories.Cache
{
  [DataContract]
  [KnownType(typeof(VKUser))]
  public sealed class UserVkRepositoryCache<T> : IVkRepositoryCache<VKUser>
  {
    [DataMember]
    private VKUser _user;

    public void AddInCache(VKUser item, object additionalData = null)
    {
      _user = item;
    }

    public VKUser GetFromCahce(object additionalData = null)
    {
      if (_user == null)
      {
        throw new Exception("User cache is NULL");
      }

      return _user;
    }

    public bool IsCached(object additionalData = null)
    {
      return _user != null;
    }

    public void ResetCache(object additionalData = null)
    {
      _user = null;
    }
  }
}
