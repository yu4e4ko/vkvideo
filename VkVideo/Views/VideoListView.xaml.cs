﻿using System.Windows;
using Microsoft.Phone.Controls;
using VkVideo.ViewModels;
using VkVideo.VkModels;
using VkVideo.Extensions;
using VkVideo.Resources;
using VkVideo.Handlers;

namespace VkVideo.Views
{
  public partial class VideoListView : PhoneApplicationPage
  {
    private VideoListViewModel _videoListViewModel;

    public VideoListView()
    {
      InitializeComponent();

      _videoListViewModel = new VideoListViewModel();
      this.DataContext = _videoListViewModel;

      SetupUserControls();
    }

    private void SetupUserControls()
    {
      // setup of albumItems
      AlbumItems.WithBinding(this.DataContext, "Albums")
        .WithItemTapHandler(OpenAlbum)
        .WithMenuItem(AppResources.Edit_album_UC, EditAlbum)
        .WithMenuSeparator()
        .WithMenuItem(AppResources.Delete_album_UC, DeleteAlbum);

      // setup of videoItems
      VideoItems.WithBinding(this.DataContext, "Video")
        .WithItemTapHandler(OpenVideo)
        .WithMenuItem(AppResources.Add_video_to_album_UC, AddVideo)
        .WithMenuSeparator()
        .WithMenuItem(AppResources.Delete_video_UC, DeleteVideo);

      // setup of catalogItems
      CatalogItems.WithBinding(this.DataContext, "CatalogRows")
        .WithItemTapHandler(OpenCatalog);

      // setup of videoSearch
      VideoSearchItems.WithBinding(this.DataContext, "SearchedVideo")
        .WithItemTapHandler(OpenSearchedVideo)
        .WithScrollUpAfterListUpdate()
        .WithMenuItem(AppResources.Add_video_to_album_UC, AddVideo);

      // setup of video searchControl
      VideoSearchControl.WithBindSearchText(this.DataContext, "VideoSearchText").WithSearchButtonEvent(VideoSearchClick);

      // setup of album searchControl
      AlbumSearchControl.WithBindSearchText(this.DataContext, "AlbumSearchText").WithSearchButtonEvent(AlbumSearchClick);

      // setup of search video searchControl
      VideoSearchSearchControl.WithBindSearchText(this.DataContext, "ExternalVideoSearchText").WithSearchButtonEvent(SearchExternalVideoClick);
    }

    private async void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
    {
      await _videoListViewModel.Init();
    }

    private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      VkVideoEventHandler.HandleBackKeyPress(sender, e);
    }

    private async void DeleteVideo(object sender, RoutedEventArgs e)
    {
      var video = ((MenuItem)sender).DataContext as VkVideoWithFile;
      await _videoListViewModel.DeleteVideo(video);
    }

    private async void VideoSearchClick(object sender, RoutedEventArgs e)
    {
      await _videoListViewModel.SearchVideo();
    }

    private async void AlbumSearchClick(object sender, RoutedEventArgs e)
    {
      await _videoListViewModel.SearchAlbums();
    }

    private void AddVideo(object sender, RoutedEventArgs e)
    {
      var video = ((MenuItem)sender).DataContext as VkVideoWithFile;
      _videoListViewModel.AddVideoToAlbum(video);
    }

    private void OpenVideo(object sender, System.Windows.Input.GestureEventArgs e)
    {
      _videoListViewModel.OpenVideo(sender as VkVideoWithFile);
    }

    private void OpenAlbum(object sender, System.Windows.Input.GestureEventArgs e)
    {
      _videoListViewModel.OpenAlbum(sender as VkAlbumWithPhoto);
    }

    private async void DeleteAlbum(object sender, RoutedEventArgs e)
    {
      var album = ((MenuItem)sender).DataContext as VkAlbumWithPhoto;
      await _videoListViewModel.DeleteAlbum(album);
    }

    private void EditAlbum(object sender, RoutedEventArgs e)
    {
      var album = ((MenuItem)sender).DataContext as VkAlbumWithPhoto;
      _videoListViewModel.EditAlbum(album);
    }

    private void OpenCatalog(object sender, System.Windows.Input.GestureEventArgs e)
    {
      _videoListViewModel.OpenCatalog(sender as VkCatalog);
    }

    private async void MoreCatalogsClick(object sender, RoutedEventArgs e)
    {
      await _videoListViewModel.MoreCatalogs();
    }

    private void OpenSearchedVideo(object sender, System.Windows.Input.GestureEventArgs e)
    {
      _videoListViewModel.OpenVideo(sender as VkVideoWithFile);
    }

    private async void SearchExternalVideoClick(object sender, RoutedEventArgs e)
    {
      await _videoListViewModel.SearchExternalVideo();
    }
  }
}