﻿using System.Collections.Generic;
using VkVideo.VkModels;

namespace VkVideo.VkRepositories.Cache
{
  public interface IAlbumVkRepositoryCache : IVkRepositoryCache<List<VkAlbumWithPhoto>>
  {
    void AddAlbumInCahce(VkAlbumWithPhoto album);

    void RemoveAlbumFromCacheById(string albumId);

    void ReplaceAlbumInCache(VkAlbumWithPhoto album, string albumId);

    bool ShouldReloadAllAbums(int albumsCount);
  }
}
