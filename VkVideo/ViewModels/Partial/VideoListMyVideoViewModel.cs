﻿using Microsoft.Phone.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VkVideo.ApplicationContext;
using VkVideo.ApplicationModels;
using VkVideo.Extensions;
using VkVideo.Helpers;
using VkVideo.Resources;
using VkVideo.VkModels;
using VkVideo.VkRepositories;

namespace VkVideo.ViewModels
{
  public partial class VideoListViewModel
  {
    private List<VkVideoWithFile> _video;
    private string _videoSearchText;

    public List<VkVideoWithFile> Video
    {
      get
      {
        return _video;
      }
      set
      {
        _video = value;
        OnPropertyChanged("Video");
      }
    }

    public string VideoSearchText
    {
      get
      {
        return _videoSearchText;
      }
      set
      {
        _videoSearchText = value;
        OnPropertyChanged("VideoSearchText");
      }
    }

    public async Task DeleteVideo(VkVideoWithFile video)
    {
      CommonHelper.ExpectedNotNullObject(video, "Delete video");

      var dialogOptions = new CustomDialogOptionsModel
      {
        Message = string.Format("{0}?", AppResources.Delete_video_UC)
      };

      var dialogResult = await this.ShowCustomDialog(dialogOptions);
      if (dialogResult.CustomMessageBoxResult == CustomMessageBoxResult.LeftButton)
      {
        await VkRepositoryResolver.Instance.DeleteVideoFromAllAlbums(video);
        await SearchVideo();
        await SearchAlbums();
      }
    }

    public async Task InitVideo()
    {
      _video = new List<VkVideoWithFile>();
      await SearchVideo();
    }

    //public async Task SearchVideo()
    //{
    //  var searchText = VideoSearchText != null ? VideoSearchText.Trim() : string.Empty;

    //  if (string.IsNullOrEmpty(searchText) == false)
    //  {
    //    // Note: VK issue (https://vk.com/support?act=show&id=20647115)
    //    // Outcome of issue:  search in list of video which are displayed

    //    /*
    //    var video = await VkRepositoryResolver.Instance.SeachVideo(searchText, true);
    //    Video = new ObservableCollection<VkVideoWithFile>(video.Where(x => x.title.ToLower().Contains(searchText)).ToList());
    //    */

    //    Video = _video.Where(x => x.title.ToLower().Contains(searchText)).ToList();
    //  }
    //  else
    //  {
    //    _video = await VkRepositoryResolver.Instance.GetV
    //  }
    //}

    public async Task SearchVideo()
    {
      var searchText = VideoSearchText != null ? VideoSearchText.Trim() : string.Empty;
      var video = await VkRepositoryResolver.Instance.GetAlbumVideoList(ApplicationConstants.MyVideosAlbumId);
      Video = video.Where(x => x.title.ToLower().Contains(searchText)).ToList();
    }

    public void OpenVideo(VkVideoWithFile video)
    {
      CommonHelper.ExpectedNotNullObject(video, "Open Video");

      VkRepositoryResolver.Instance.CurrentVideo = video;
      NaviagationHelper.Navigate("/Views/VideoView.xaml");
    }
  }
}
