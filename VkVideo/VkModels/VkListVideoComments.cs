﻿using System.Collections.Generic;
using VK.WindowsPhone.SDK.API.Model;

namespace VkVideo.VkModels
{
  public class VkListVideoComments<T> : VKList<T>
  {
    public VkListVideoComments()
    {
      Profiles = new List<VKUser>();
      items = new List<T>();
    }

    public List<VKUser> Profiles { get; set; }
  }
}
