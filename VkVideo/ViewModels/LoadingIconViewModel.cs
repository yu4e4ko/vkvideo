﻿using System.Windows;

namespace VkVideo.ViewModels
{
  public class LoadingIconViewModel : BaseViewModel
  {
    private Visibility _loadingVisibility;

    public LoadingIconViewModel()
    {
      _loadingVisibility = Visibility.Collapsed;
    }

    public Visibility LoadingVisibility
    {
      get
      {
        return _loadingVisibility;
      }
      set
      {
        _loadingVisibility = value;
        OnPropertyChanged("LoadingVisibility");
      }
    }
  }
}
