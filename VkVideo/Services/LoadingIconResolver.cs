﻿using VkVideo.ViewModels;

namespace VkVideo.Services
{
  public class LoadingIconResolver
  {
    private static LoadingIconViewModel _instance;

    public static LoadingIconViewModel Instance
    {
      get
      {
        if (_instance == null)
        {
          _instance = new LoadingIconViewModel();
        }

        return _instance;
      }
    }
  }
}
