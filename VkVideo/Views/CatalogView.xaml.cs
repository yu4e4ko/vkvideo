﻿using System.Windows;
using Microsoft.Phone.Controls;
using VkVideo.ViewModels;
using VkVideo.VkModels;
using VkVideo.Extensions;
using VkVideo.Resources;
using VkVideo.Handlers;

namespace VkVideo.Views
{
  public partial class CatalogView : PhoneApplicationPage
  {
    private readonly CatalogViewModel _catalogViewModel;

    public CatalogView()
    {
      InitializeComponent();

      _catalogViewModel = new CatalogViewModel();
      this.DataContext = _catalogViewModel;

      VideoListControl.WithBinding(this.DataContext, "Video")
        .WithItemTapHandler(Image_Tap)
        .WithMenuItem(AppResources.Add_video_to_album_UC, MenuAddVideo_Click);
    }

    private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
    {
      _catalogViewModel.Init();
    }

    private async void BtnMoreVideo_Click(object sender, RoutedEventArgs e)
    {
      await _catalogViewModel.MoreVideo();
    }

    private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
    {
      _catalogViewModel.OpenVideo(sender as VkVideoWithFile);
    }

    private void MenuAddVideo_Click(object sender, RoutedEventArgs e)
    {
      var video = ((MenuItem)sender).DataContext as VkVideoWithFile;
      _catalogViewModel.AddVideoToAlbum(video);
    }

    private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      VkVideoEventHandler.HandleBackKeyPress(sender, e);
    }
  }
}