﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Controls;
using VkVideo.Extensions;
using VkVideo.VkModels;
using VkVideo.VkRepositories;

namespace VkVideo.ViewModels
{
  public partial class VideoListViewModel
  {
    private List<VkVideoWithFile> _searchedVideo;
    private string _externalVideoSearchText;

    public List<VkVideoWithFile> SearchedVideo
    {
      get
      {
        return _searchedVideo;
      }
      set
      {
        _searchedVideo = value;
        OnPropertyChanged("SearchedVideo");
      }
    }

    public string ExternalVideoSearchText
    {
      get
      {
        return _externalVideoSearchText;
      }
      set
      {
        _externalVideoSearchText = value;
        OnPropertyChanged("ExternalVideoSearchText");
      }
    }

    public async Task SearchExternalVideo()
    {
      var searchCriteria = ExternalVideoSearchText != null ? ExternalVideoSearchText.Trim() : string.Empty;
      if (string.IsNullOrEmpty(searchCriteria))
      {
        SearchedVideo = new List<VkVideoWithFile>();
        return;
      }

      SearchedVideo = await VkRepositoryResolver.Instance.SeachVideo(searchCriteria);
    }
  }
}
