﻿using System.Windows;
using Microsoft.Phone.Controls;
using VkVideo.ViewModels;
using VkVideo.VkRepositories;
using System.Windows.Controls;
using VkVideo.VkModels;
using VkVideo.Extensions;
using VkVideo.Resources;
using VkVideo.Handlers;

namespace VkVideo.Views
{
  public partial class AlbumView : PhoneApplicationPage
  {
    private readonly AlbumViewModel _albumViewModel;

    public AlbumView()
    {
      InitializeComponent();

      var currentAlbum = VkRepositoryResolver.Instance.CurrentAlbum;
      _albumViewModel = new AlbumViewModel(currentAlbum);

      DataContext = _albumViewModel;

      VideoListControl.WithBinding(this.DataContext, "Video")
        .WithItemTapHandler(Image_Tap)
        .WithMenuItem(AppResources.Add_video_to_album_UC, AddVideoMenuItem_Click)
        .WithMenuSeparator()
        .WithMenuItem(AppResources.Delete_video_from_album_UC, MenuDeleteVideoFromAlbum_Click)
        .WithMenuItem(AppResources.Delete_video_UC, MenuDeleteVideo_Click);

      VideoSearchControl.WithBindSearchText(this.DataContext, "SearchText").WithSearchButtonEvent(BtnVideoSearch_Click);
    }

    private async void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
    {
      await _albumViewModel.Init();
    }

    private async void BtnVideoSearch_Click(object sender, RoutedEventArgs e)
    {
      await _albumViewModel.SearchVideo();
    }

    private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
    {
      _albumViewModel.OpenVideo(sender as VkVideoWithFile);
    }

    private async void MenuDeleteVideoFromAlbum_Click(object sender, RoutedEventArgs e)
    {
      var video = ((MenuItem)sender).DataContext as VkVideoWithFile;
      await _albumViewModel.DeleteVideoFromAlbum(video);
    }

    private async void MenuDeleteVideo_Click(object sender, RoutedEventArgs e)
    {
      var video = ((MenuItem)sender).DataContext as VkVideoWithFile;
      await _albumViewModel.DeleteVideo(video);
    }

    private void AddVideoMenuItem_Click(object sender, RoutedEventArgs e)
    {
      var video = ((MenuItem)sender).DataContext as VkVideoWithFile;
      _albumViewModel.AddVideoToAlbum(video);
    }

    private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      VkVideoEventHandler.HandleBackKeyPress(sender, e);
    }
  }
}