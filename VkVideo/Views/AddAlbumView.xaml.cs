﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using VkVideo.ViewModels;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Phone.Shell;
using VkVideo.Resources;
using VkVideo.ApplicationContext;
using VkVideo.Handlers;

namespace VkVideo.Views
{
  public partial class AddAlbumView : PhoneApplicationPage
  {
    private readonly AddAlbumViewModel _addAlbumViewModel;

    public AddAlbumView()
    {
      InitializeComponent();
      _addAlbumViewModel = new AddAlbumViewModel();
      this.DataContext = _addAlbumViewModel; 
    }

    public void BuildApplicationBar()
    {
      ApplicationBar = new ApplicationBar();

      // Create a new button and set the text value to the localized string from AppResources.
      var saveButton = new ApplicationBarIconButton(new Uri(ApplicationConstants.SaveMenuIconPath, UriKind.Relative));
      saveButton.Text = AppResources.Save_UC;
      saveButton.Click += SaveButton_Click;
      ApplicationBar.Buttons.Add(saveButton);

      if (_addAlbumViewModel.AddMode == false)
      {
        var deleteButton = new ApplicationBarIconButton(new Uri(ApplicationConstants.DeleteMenuIconPath, UriKind.Relative));
        deleteButton.Text = AppResources.Delete_album_UC;
        deleteButton.Click += DeleteButton_Click;
        ApplicationBar.Buttons.Add(deleteButton);
      }
    }

    private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
    {
      _addAlbumViewModel.Init();
      BuildApplicationBar();

      TxtAlbumName.SelectionStart = string.IsNullOrEmpty(_addAlbumViewModel.AlbumName) == false ? _addAlbumViewModel.AlbumName.Length : 0;
    }

    private async void DeleteButton_Click(object sender, EventArgs e)
    {
      await _addAlbumViewModel.DeleteAlbum();
    }

    private async void SaveButton_Click(object sender, EventArgs e)
    {
      // Note: update trigger 
      var focusObj = FocusManager.GetFocusedElement();
      if (focusObj != null && focusObj is TextBox)
      {
        var binding = (focusObj as TextBox).GetBindingExpression(TextBox.TextProperty);
        binding.UpdateSource();
      }

      await _addAlbumViewModel.Save();
    }

    private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      VkVideoEventHandler.HandleBackKeyPress(sender, e);
    }
  }
}