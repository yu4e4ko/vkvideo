﻿using Microsoft.Phone.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using VkVideo.ApplicationModels;
using VkVideo.Extensions;
using VkVideo.Helpers;
using VkVideo.Resources;
using VkVideo.VkModels;
using VkVideo.VkRepositories;

namespace VkVideo.ViewModels
{
  public partial class VideoListViewModel
  {
    private List<VkAlbumWithPhoto> _albums;
    private string _albumSearchText;

    public List<VkAlbumWithPhoto> Albums
    {
      get
      {
        return _albums;
      }
      set
      {
        _albums = value;
        OnPropertyChanged("Albums");
      }
    }

    public string AlbumSearchText
    {
      get
      {
        return _albumSearchText;
      }
      set
      {
        _albumSearchText = value;
        OnPropertyChanged("AlbumSearchText");
      }
    }

    public async Task SearchAlbums()
    {
      var searchText = AlbumSearchText != null ? AlbumSearchText.Trim() : string.Empty;
      var albums = await VkRepositoryResolver.Instance.GetCurrentUserAlbums();
      Albums = albums.Where(x => x.title.ToLower().Contains(searchText)).ToList();
    }

    public void OpenAlbum(VkAlbumWithPhoto album)
    {
      CommonHelper.ExpectedNotNullObject(album, "Open Album");

      VkRepositoryResolver.Instance.CurrentAlbum = album;
      NaviagationHelper.Navigate("/Views/AlbumView.xaml");
    }

    public async Task DeleteAlbum(VkAlbumWithPhoto album)
    {
      if (await DeleteAlbumBase(album))
      {
        await SearchAlbums();
      }
    }

    public void EditAlbum(VkAlbumWithPhoto album)
    {
      VkRepositoryResolver.Instance.CurrentAlbum = album;
      NaviagationHelper.Navigate("/Views/AddAlbumView.xaml");
    }
  }
}
