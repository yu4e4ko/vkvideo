﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using VkVideo.Helpers;
using VkVideo.VkModels;
using VkVideo.VkRepositories;

namespace VkVideo.ViewModels
{
  public partial class VideoListViewModel
  {
    private const int MaxCountOfCatalogs = 50;

    private Visibility _catalogMoreButtonVisibility;
    private VkCatalogs _catalogs;
    private ObservableCollection<VkCatalog> _catalogRows;
    private bool _moreButtonEnabled;

    public ObservableCollection<VkCatalog> CatalogRows
    {
      get
      {
        return _catalogRows;
      }
      set
      {
        _catalogRows = value;
        OnPropertyChanged("CatalogRows");
      }
    }

    public Visibility CatalogMoreButtonVisibility
    {
      get
      {
        return _catalogMoreButtonVisibility;
      }
      set
      {
        _catalogMoreButtonVisibility = value;
        OnPropertyChanged("CatalogMoreButtonVisibility");
      }
    }

    public bool MoreButtonEnabled
    {
      get
      {
        return _moreButtonEnabled;
      }
      set
      {
        _moreButtonEnabled = value;
        OnPropertyChanged("MoreButtonEnabled");
      }
    }

    public void OpenCatalog(VkCatalog catalog)
    {
      CommonHelper.ExpectedNotNullObject(catalog, "Open Catalog");

      VkRepositoryResolver.Instance.CurrectCatalog = catalog;
      NaviagationHelper.Navigate("/Views/CatalogView.xaml");
    }

    public async Task MoreCatalogs()
    {
      if (CatalogRows.Any() == false || CatalogRows.Count >= MaxCountOfCatalogs)
      {
        return;
      }

      MoreButtonEnabled = false;
      var next = _catalogs.Next;
      var response = await VkRepositoryResolver.Instance.GetCatalogs(next);

      CatalogRows = new ObservableCollection<VkCatalog>(response.Items);

      MoreButtonEnabled = true;
      SetMoreCatalogsButtonVisibility();
    }

    private void SetMoreCatalogsButtonVisibility()
    {
      CatalogMoreButtonVisibility = CatalogRows.Count >= MaxCountOfCatalogs ? Visibility.Collapsed : Visibility.Visible;
    }
  }
}
