﻿using System.Windows.Media;

namespace VkVideo.Resources
{
  public class StyleResources
  {
    private static Color _foregroundColor = Color.FromArgb(0xFF, 0xF7, 0xF7, 0xF7);
    private static Color _backgroundColor = Color.FromArgb(0xFF, 0x3F, 0x62, 0x88);
    private static Color _borderColor = Color.FromArgb(0xFF, 0x25, 0x49, 0x6F);
    private static Color _additionalRecordsColor = Color.FromArgb(0xFF, 0x00, 0xFF, 0xE9);

    private static SolidColorBrush _mainForegroundBrush = new SolidColorBrush(_foregroundColor);
    private static SolidColorBrush _mainBackgroundBrush = new SolidColorBrush(_backgroundColor);
    private static SolidColorBrush _mainBorderBrush = new SolidColorBrush(_borderColor);
    private static SolidColorBrush _additionalRecordsBrush = new SolidColorBrush(_additionalRecordsColor);

    public static SolidColorBrush MainForegroundBrush
    {
      get
      {
        return _mainForegroundBrush;
      }
    }

    public static SolidColorBrush MainBackgroundBrush
    {
      get
      {
        return _mainBackgroundBrush;
      }
    }

    public static SolidColorBrush MainBorderBrush
    {
      get
      {
        return _mainBorderBrush;
      }
    }

    public static SolidColorBrush AdditionalRecordsBrush
    {
      get
      {
        return _additionalRecordsBrush;
      }
    }
  }
}
