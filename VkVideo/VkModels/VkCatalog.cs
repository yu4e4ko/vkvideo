﻿using System.Collections.Generic;
using System.Linq;
using VkVideo.ApplicationContext;

namespace VkVideo.VkModels
{
  public class VkCatalog
  {
    public VkCatalog()
    {
      Items = new List<VkVideoWithFile>();
    }

    public string id { get; set; }

    public string Next { get; set; }

    public string Name { get; set; }

    public string Type { get; set; }

    public bool Can_Hide { get; set; }

    public List<VkVideoWithFile> Items { get; set; }
    
    public string photo_320
    {
      get
      {
        if (Items.Any())
        {
          return Items.First().photo_320;
        }

        return ApplicationConstants.EmptyImagePath;
      }
    }

    // Note: for bindings in custom controls
    public string title
    {
      get
      {
        return Name;
      }
    }
  }
}
