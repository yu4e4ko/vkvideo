﻿using VK.WindowsPhone.SDK.API;

namespace VkVideo.ApplicationModels
{
  public class CustomDialogOptionsModel
  {
    public string Message { get; set; }

    public bool ContainsInput { get; set; }

    public string Title { get; set; }

    public string InputText { get; set; }

    public int? InputTextMaxLength { get; set; }

    public VKCaptchaUserRequest CaptchaUserRequest { get; set; }
  }
}
