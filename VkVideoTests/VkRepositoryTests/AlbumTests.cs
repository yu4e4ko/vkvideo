﻿using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using VK.WindowsPhone.SDK.API;
using VK.WindowsPhone.SDK.API.Model;
using VkVideo.VkModels;
using VkVideo.VkRepositories;

namespace VkVideoTests.VkRepositoryTests
{
  [TestFixture]
  public class AlbumTests
  {
    private const int DefaultVideoListCount = 100;
    private const string DefaultAlbumId = "12345";

    private Mock<IVkApiProvider> _mock;
    private VkRepository _vkRepository;
    private VKUser _user;
    private List<VkVideoWithFile> _videoList;
    private VKBackendResult<VKList<int>> _deleteVideoFromAlbumBackendResult;

    [OneTimeSetUp]
    public void Init()
    {
      _videoList = new List<VkVideoWithFile>();

      for (var i = 0; i < DefaultVideoListCount; i++)
      {
        _videoList.Add(new VkVideoWithFile());
      }

      _user = new VKUser
      {
        id = 555
      };

      _deleteVideoFromAlbumBackendResult = new VKBackendResult<VKList<int>>
      {
        ResultCode = VKResultCode.Succeeded
      };
    }

    [SetUp]
    public void SetUp()
    {
      _mock = new Mock<IVkApiProvider>();
      _mock.Setup(r => r.LoadUser()).ReturnsAsync(_user);
      _mock.Setup(r => r.VideoGet(It.IsAny<string>())).ReturnsAsync(_videoList);
      _mock.Setup(r => r.DeleteVideoFromAlbum(It.IsAny<VkVideoWithFile>(), It.IsAny<string[]>()))
        .ReturnsAsync(_deleteVideoFromAlbumBackendResult);

      _vkRepository = new VkRepository(_mock.Object);
    }

    [Test]
    public async Task AlbumTests_ShouldBeInvokeLoadUserVideoOnce()
    {
      // Arrange
      // Act
      var video = await _vkRepository.GetAlbumVideoList(DefaultAlbumId);
      var albumVideo = await _vkRepository.GetAlbumVideoList(DefaultAlbumId);

      // Assert
      _mock.Verify(r => r.VideoGet(DefaultAlbumId), Times.Once);
    }

    [Test]
    public async Task AlbumTests_ShouldBeInvokeLoadUserVideoTwice()
    {
      // Arrange
      var someAlbumId = "123";
      var expectedInvokeCount = 2;

      // Act
      var video = await _vkRepository.GetAlbumVideoList(DefaultAlbumId);
      var albumVideo = await _vkRepository.GetAlbumVideoList(someAlbumId);

      // Assert
      _mock.Verify(r => r.VideoGet(It.IsAny<string>()), Times.Exactly(expectedInvokeCount));
    }

    [Test]
    [Ignore("Cache logic was updated")]
    public async Task AlbumTests_ShouldBeCacheResetAfterDeleteVideoFromAlbum()
    {
      // Arrange
      var expectedInvokeCount = 2;

      // Act
      var video = await _vkRepository.GetAlbumVideoList(DefaultAlbumId);
      await _vkRepository.DeleteVideoFromAlbum(It.IsAny<VkVideoWithFile>(), DefaultAlbumId);
      video = await _vkRepository.GetAlbumVideoList(DefaultAlbumId);

      // Assert
      _mock.Verify(r => r.VideoGet(It.IsAny<string>()), Times.Exactly(expectedInvokeCount));
    }
  }
}
