﻿using System;
using System.Runtime.Serialization;
using VkVideo.VkModels;

namespace VkVideo.VkRepositories.Cache
{
  [DataContract]
  [KnownType(typeof(VkCatalogs))]
  public sealed class CatalogsVkRepositoryCache<T> : IVkRepositoryCache<VkCatalogs>
  {
    [DataMember]
    private VkCatalogs _cachedCatalogs;

    public void AddInCache(VkCatalogs item, object additionalData = null)
    {
      _cachedCatalogs = item;
    }

    public VkCatalogs GetFromCahce(object additionalData = null)
    {
      if (_cachedCatalogs == null)
      {
        throw new Exception("Catalogs cache is NULL");
      }

      return _cachedCatalogs;
    }

    public bool IsCached(object additionalData = null)
    {
      return _cachedCatalogs != null;
    }

    public void ResetCache(object additionalData = null)
    {
      _cachedCatalogs = null;
    }
  }
}
