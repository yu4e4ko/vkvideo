﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using VK.WindowsPhone.SDK.API.Model;
using VkVideo.Resources;

namespace VkVideo.VkModels
{
  public class VkCommentWithProfile : VKComment
  {
    public VkCommentWithProfile()
    {
    }

    public VkCommentWithProfile(VKComment comment)
    {
      this.attachments = comment.attachments;
      this.date = comment.date;
      this.from_id = comment.from_id;
      this.id = comment.id;
      this.reply_to_comment = comment.reply_to_comment;
      this.reply_to_user = comment.reply_to_user;
      this.text = comment.text;
    }

    public long ProfileId { get; set; }

    public string Photo_50 { get; set; }

    public string Photo_100 { get; set; }

    public string Photo_200 { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string FullName
    {
      get
      {
        var commentDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Convert.ToDouble(date)).ToLocalTime();

        return string.Format(
          "{0} {1} ({2})", 
          this.FirstName ?? string.Empty, 
          this.LastName ?? string.Empty,
          commentDate.ToString("g", Thread.CurrentThread.CurrentCulture));
      }
    }

    public string FormattedText
    {
      get
      {
        if (string.IsNullOrEmpty(text) && attachments != null && attachments.Any())
        {
          return string.Format("*** {0} ***", AppResources.Attachments_do_not_supported_UC);
        }

        if (string.IsNullOrEmpty(text) == false && text.StartsWith("["))
        {
          var regex = new Regex("([\\[]id[\\d]+[\\|])([a-zA-Z0-9а-яА-ЯёЁ]+)([\\]])");
          var match = regex.Match(text);

          var textWithoutNameIndex = text.IndexOf("],");

          if (match.Success && match.Groups.Count > 1 &&  textWithoutNameIndex != -1)
          {
            return string.Format("{0},{1}", match.Groups[2].Value, text.Substring(textWithoutNameIndex + 2));
          }

          return text;
        }

        return text;
      }
    }
  }
}
