﻿using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VkVideo.VkModels;
using VkVideo.VkRepositories;

namespace VkVideoTests.VkRepositoryTests
{
  [TestFixture]
  public class CatalogsTests
  {
    private Mock<IVkApiProvider> _mock;
    private VkRepository _vkRepository;

    private List<VkVideoWithFile> _fakeVideos;
    private VkCatalogs _vkCatalogs;
    private VkCatalog _vkCatalog;

    [OneTimeSetUp]
    public void Init()
    {
      _vkCatalogs = new VkCatalogs();

      _vkCatalog = new VkCatalog
      {
        id = "someCatalogId"
      };

      _fakeVideos = new List<VkVideoWithFile>();

      for (var i = 0; i < 10; i++)
      {
        _fakeVideos.Add(new VkVideoWithFile());
      }

      _vkCatalog.Items = _fakeVideos;
      _vkCatalogs.Items.Add(_vkCatalog);
    }

    [SetUp]
    public void SetUp()
    {
      var videos = new List<VkVideoWithFile>();

      for(var i = 0; i < 10; i++)
      {
        videos.Add(new VkVideoWithFile());
      }

      var fakeCatalog = new VkCatalog
      {
        Items = videos
      };

      _mock = new Mock<IVkApiProvider>();
      _mock.Setup(r => r.GetCatalogItems(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(fakeCatalog);
      _mock.Setup(r => r.GetCatalogs(It.IsAny<string>())).ReturnsAsync(_vkCatalogs);

      _vkRepository = new VkRepository(_mock.Object);
    }
  }
}
