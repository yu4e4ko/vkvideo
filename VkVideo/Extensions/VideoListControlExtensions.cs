﻿using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using VkVideo.ApplicationContext;
using VkVideo.UserControls;

namespace VkVideo.Extensions
{
  public static class VideoListControlExtensions
  {
    public static VideoListControl WithMenuItem(
      this VideoListControl @this, 
      string header, 
      RoutedEventHandler eventHandler)
    {
      var menuItem = new MenuItem
      {
        Header = header,
        Height = ApplicationConstants.MenuItemHeight
      };

      menuItem.Click += eventHandler;
      @this.MenuItems.Add(menuItem);

      return @this;
    }

    public static VideoListControl WithMenuSeparator(this VideoListControl @this)
    {
      @this.MenuItems.Add(new Separator());
      return @this;
    }

    public static VideoListControl WithItemTapHandler(
      this VideoListControl @this,
      EventHandler<System.Windows.Input.GestureEventArgs> eventHandler)
    {
      @this.ItemTapHanlder += eventHandler;
      return @this;
    }

    public static VideoListControl WithBinding(
      this VideoListControl @this,
      object source,
      string pathName)
    {
      var binding = new Binding
      {
        Mode = BindingMode.OneWay,
        Source = source,
        Path = new PropertyPath(pathName)
      };

      @this.MainList.SetBinding(ListBox.ItemsSourceProperty, binding);

      return @this;
    }

    public static VideoListControl WithScrollUpAfterListUpdate(this VideoListControl @this)
    {
      @this.MainList.ItemContainerGenerator.ItemsChanged += @this.ScrollUp;
      return @this;
    }
  }
}
