﻿using Microsoft.Phone.Controls;

namespace VkVideo.VkModels
{
  public class VkCustomDialogModel
  {
    public CustomMessageBoxResult CustomMessageBoxResult { get; set; }

    public string InputText { get; set; }

    public int? Key { get; set; }
  }
}
