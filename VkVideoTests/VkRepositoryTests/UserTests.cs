﻿using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using VK.WindowsPhone.SDK.API.Model;
using VkVideo.VkRepositories;

namespace VkVideoTests.VkRepositoryTests
{
  [TestFixture]
  public class UserTests
  {
    private const long DefaultUserId = 555;
    private VKUser _user;

    private Mock<IVkApiProvider> _mock;
    private VkRepository _vkRepository;

    [OneTimeSetUp]
    public void Init()
    {
      _user = new VKUser
      {
        id = DefaultUserId
      };
    }

    [SetUp]
    public void SetUp()
    {
      _mock = new Mock<IVkApiProvider>();
      _mock.Setup(r => r.LoadUser()).ReturnsAsync(_user);
      _vkRepository = new VkRepository(_mock.Object);
    }

    [Test]
    public async Task UserTests_ShouldBeGettedUserWithDefaultId()
    {
      // Arrange
      var expectedUserId = 555;

      // Act
      var user = await _vkRepository.GetCurrentUser();

      // Assert
      Assert.AreEqual(expectedUserId, user.id);
    }

    [Test]
    public async Task UserTests_ShouldBeInvokeLoadUserOnce()
    {
      // Act
      var user = await _vkRepository.GetCurrentUser();
      user = await _vkRepository.GetCurrentUser();
      user = await _vkRepository.GetCurrentUser();

      // Assert
      _mock.Verify(r => r.LoadUser(), Times.Once);
    }
  }
}
