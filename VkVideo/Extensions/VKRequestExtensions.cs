﻿using System.Threading.Tasks;
using System.Windows;
using VK.WindowsPhone.SDK.API;
using VkVideo.Helpers;
using VkVideo.Services;

namespace VkVideo.Extensions
{
  public static class VKRequestExtensions
  {
    public static async Task<VKBackendResult<T>> GetResponseAsync<T>(
      this VKRequestParameters vkRequestParameters, 
      string senderName) 
    {
      var request = new VKRequest(vkRequestParameters);
      var taskCompletionSource = new TaskCompletionSource<VKBackendResult<T>>();

      request.Dispatch<T>((res) => { taskCompletionSource.TrySetResult(res); });

      LoadingIconService.Instance.ShowLoadingDialog();
      var response = await taskCompletionSource.Task;
      LoadingIconService.Instance.HideLoadingDialog();

      if (response.ResultCode == VKResultCode.CaptchaControlCancelled)
      {
        MessageBox.Show("Capture needed error");
        return response;
      }

      // Note: it's not critical error
      if (response.ResultCode == VKResultCode.NotAllowed)
      {
        CommonHelper.ShowError("NOT ALLOWED", true);
        return response;
      }

      if (response.Error != null)
      {
        var errorMessage = string.Format(
          "VK Api error: ({0}) {1}\nSender: \"{2}\"", 
          response.Error.error_code, 
          response.Error.error_msg,
          senderName ?? string.Empty);

        CommonHelper.ShowError(errorMessage);
      }

      if (response.ResultCode != VKResultCode.Succeeded)
      {
        CommonHelper.ShowError(string.Format(
          "Result code contains error: {0}\nSender: \"{1}\"", 
          response.ResultCode.ToString(), 
          senderName ?? string.Empty));
      }

      return response;
    }
  }
}
