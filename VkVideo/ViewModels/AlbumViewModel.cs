﻿using Microsoft.Phone.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VkVideo.VkRepositories;
using VkVideo.Extensions;
using VkVideo.Helpers;
using VkVideo.VkModels;
using VkVideo.Resources;
using VkVideo.ApplicationModels;

namespace VkVideo.ViewModels
{
  public class AlbumViewModel : BaseViewModel
  {
    private readonly VkAlbumWithPhoto _album;
    private IList<VkVideoWithFile> _video;
    private string _albumName;
    private string _searchText;

    public AlbumViewModel(VkAlbumWithPhoto album)
    {
      _album = album;
      _video = new List<VkVideoWithFile>();
    }

    #region properties

    public IList<VkVideoWithFile> Video
    {
      get
      {
        return _video;
      }
      set
      {
        _video = value;
        OnPropertyChanged("Video");
      }
    }

    public string AlbumName
    {
      get
      {
        return _albumName;
      }
      set
      {
        _albumName = value;
        OnPropertyChanged("AlbumName");
      }
    }

    public string SearchText
    {
      get
      {
        return _searchText;
      }
      set
      {
        _searchText = value;
        OnPropertyChanged("SearchText");
      }
    }

    #endregion

    #region methods

    public async Task Init()
    {
      AlbumName = _album.title;
      await SearchVideo();
    }

    public async Task SearchVideo()
    {
      var searchText = SearchText != null ? SearchText.Trim() : string.Empty;
      var video = await VkRepositoryResolver.Instance.GetAlbumVideoList(_album.id);
      Video = video.Where(x => x.title.ToLower().Contains(searchText)).ToList();
    }

    public void OpenVideo(VkVideoWithFile video)
    {
      CommonHelper.ExpectedNotNullObject(video, "Open video");

      VkRepositoryResolver.Instance.CurrentVideo = video;
      NaviagationHelper.Navigate("/Views/VideoView.xaml");
    }

    public async Task DeleteVideo(VkVideoWithFile video)
    {
      CommonHelper.ExpectedNotNullObject(video, "Delete video");

      var dialogOptions = new CustomDialogOptionsModel
      {
        Message = string.Format("{0}?", AppResources.Delete_video_UC)
      };

      var dialogResult = await this.ShowCustomDialog(dialogOptions);
      if (dialogResult.CustomMessageBoxResult == CustomMessageBoxResult.LeftButton)
      {
        await VkRepositoryResolver.Instance.DeleteVideoFromAllAlbums(video);
        await SearchVideo();
      }
    }

    public async Task DeleteVideoFromAlbum(VkVideoWithFile video)
    {
      CommonHelper.ExpectedNotNullObject(video, "Delete video from album");

      var dialogOptions = new CustomDialogOptionsModel
      {
        Message = string.Format("{0}?", AppResources.Delete_video_from_album_UC)
      };

      var dialogResult = await this.ShowCustomDialog(dialogOptions);
      if (dialogResult.CustomMessageBoxResult == CustomMessageBoxResult.LeftButton)
      {
        await VkRepositoryResolver.Instance.DeleteVideoFromAlbum(video, _album.id);
        await SearchVideo();
      }
    }

    #endregion
  }
}
