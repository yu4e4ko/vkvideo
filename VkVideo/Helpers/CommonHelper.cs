﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using VK.WindowsPhone.SDK.API;
using VkVideo.ApplicationModels;
using VkVideo.Extensions;
using VkVideo.Services;

namespace VkVideo.Helpers
{
  public class CommonHelper
  {
    public static void ShowError(string errorMessage, bool withoutRedirect = false)
    {
      MessageBox.Show("Error: " + errorMessage ?? string.Empty);
      LoadingIconService.Instance.HideLoadingDialogForce();

      if (withoutRedirect == false)
      {
        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Views/LoginView.xaml", UriKind.Relative));
      }
    }

    public static void ExpectedNotNullObject(object target, string note = "")
    {
      if (target == null)
      {
        throw new Exception(string.Format("Object should be not NULL! {0}", note));
      }
    }

    public static T FindChildOfType<T>(DependencyObject root) where T : class
    {
      var queue = new Queue<DependencyObject>();
      queue.Enqueue(root);

      while (queue.Count > 0)
      {
        var current = queue.Dequeue();
        for (int i = VisualTreeHelper.GetChildrenCount(current) - 1; 0 <= i; i--)
        {
          var child = VisualTreeHelper.GetChild(current, i);
          var typedChild = child as T;
          if (typedChild != null)
          {
            return typedChild;
          }

          queue.Enqueue(child);
        }
      }
      return null;
    }

    public static async void CaptchaRequest(VKCaptchaUserRequest captchaUserRequest, Action<VKCaptchaUserResponse> action)
    {
      var dialogOptions = new CustomDialogOptionsModel
      {
        CaptchaUserRequest = captchaUserRequest
      };

      var customMessageBox = await UIExtensions.ShowCustomDialog(null, dialogOptions);

      if (customMessageBox.CustomMessageBoxResult == CustomMessageBoxResult.LeftButton)
      {
        action.Invoke(new VKCaptchaUserResponse()
        {
          EnteredString = customMessageBox.InputText,
          IsCancelled = false,
          Request = captchaUserRequest
        });
      }
      else
      {
        action.Invoke(new VKCaptchaUserResponse()
        {
          IsCancelled = true,
          Request = captchaUserRequest
        });
      }
    }
  }
}
