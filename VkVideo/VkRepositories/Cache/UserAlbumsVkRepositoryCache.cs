﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using VkVideo.VkModels;

namespace VkVideo.VkRepositories.Cache
{
  [DataContract]
  [KnownType(typeof(List<VkAlbumWithPhoto>))]
  public sealed class UserAlbumsVkRepositoryCache : IAlbumVkRepositoryCache
  {
    [DataMember]
    private List<VkAlbumWithPhoto> _cachedUserAlbums;

    public void AddAlbumInCahce(VkAlbumWithPhoto album)
    {
      if (_cachedUserAlbums == null)
      {
        return;
      }

      _cachedUserAlbums.Insert(0, album);
    }

    public void AddInCache(List<VkAlbumWithPhoto> item, object additionalData = null)
    {
      _cachedUserAlbums = item;
    }

    public List<VkAlbumWithPhoto> GetFromCahce(object additionalData = null)
    {
      if (_cachedUserAlbums == null)
      {
        throw new Exception("UserAlbums cache is NULL");
      }

      return _cachedUserAlbums;
    }

    public bool IsCached(object additionalData = null)
    {
      return _cachedUserAlbums != null;
    }

    public bool ShouldReloadAllAbums(int albumsCount)
    {
      if (_cachedUserAlbums == null)
      {
        return true;
      }

      // Note: albums are loaded by 100 items
      return albumsCount > (_cachedUserAlbums.Count / 100 + 1);
    }

    public void RemoveAlbumFromCacheById(string albumId)
    {
      if (_cachedUserAlbums == null)
      {
        return;
      }

      var album = _cachedUserAlbums.FirstOrDefault(r => r.id == albumId);

      if (album != null)
      {
        _cachedUserAlbums.Remove(album);
      } 
    }

    public void ReplaceAlbumInCache(VkAlbumWithPhoto album, string albumId)
    {
      if (_cachedUserAlbums == null)
      {
        return;
      }

      var cachedAlbumIndex = _cachedUserAlbums.FindIndex(r => r.id == albumId);

      if (cachedAlbumIndex > -1)
      {
        _cachedUserAlbums[cachedAlbumIndex] = album;
      }
    }

    public void ResetCache(object additionalData = null)
    {
      _cachedUserAlbums = null;
    }
  }
}
