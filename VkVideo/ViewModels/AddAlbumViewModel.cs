﻿using System.Threading.Tasks;
using System.Windows;
using VkVideo.VkRepositories;
using VkVideo.Helpers;
using VkVideo.Resources;
using VK.WindowsPhone.SDK.API.Model;

namespace VkVideo.ViewModels
{
  public class AddAlbumViewModel : BaseViewModel
  {
    private bool _onlyMeAccess;
    private bool _friendsAdnFriendsOfFriendsAccess;
    private bool _friendsAccess;
    private bool _allUsersAccess;
    private string _albumName;
    private bool _addMode;

    #region properties

    public bool OnlyMeAccess
    {
      get
      {
        return _onlyMeAccess;
      }
      set
      {
        _onlyMeAccess = value;
        OnPropertyChanged("OnlyMeAccess");
      }
    }

    public bool FriendsAdnFriendsOfFriendsAccess
    {
      get
      {
        return _friendsAdnFriendsOfFriendsAccess;
      }
      set
      {
        _friendsAdnFriendsOfFriendsAccess = value;
        OnPropertyChanged("FriendsAdnFriendsOfFriendsAccess");
      }
    }

    public bool FriendsAccess
    {
      get
      {
        return _friendsAccess;
      }
      set
      {
        _friendsAccess = value;
        OnPropertyChanged("FriendsAccess");
      }
    }

    public bool AllUsersAccess
    {
      get
      {
        return _allUsersAccess;
      }
      set
      {
        _allUsersAccess = value;
        OnPropertyChanged("AllUsersAccess");
      }
    }

    public string AlbumName
    {
      get
      {
        return _albumName;
      }
      set
      {
        _albumName = value;
        OnPropertyChanged("AlbumName");
      }
    }

    public bool AddMode
    {
      get
      {
        return _addMode;
      }
    }

    #endregion

    #region methods

    public void Init()
    {
      var currentAlbum = VkRepositoryResolver.Instance.CurrentAlbum;

      if (currentAlbum == null)
      {
        OnlyMeAccess = true;
        _addMode = true;
      }
      else
      {
        AlbumName = currentAlbum.title;
        SetAccessPropertiesByPrivacy(currentAlbum.Privacy);
      }
    }

    public async Task DeleteAlbum()
    {
      if (await DeleteAlbumBase(VkRepositoryResolver.Instance.CurrentAlbum))
      {
        NaviagationHelper.GoBack();
      }
    }

    public async Task Save()
    {
      var albumName = AlbumName != null ? AlbumName.Trim() : string.Empty;
      var privacy = GetPrivacyByAccessProperties();

      if (ValidateAlbumName(albumName))
      {
        if (_addMode)
        {
          await VkRepositoryResolver.Instance.AddAlbum(albumName, privacy);
        }
        else
        {
          await VkRepositoryResolver.Instance.EditAlbum(VkRepositoryResolver.Instance.CurrentAlbum.id, albumName, privacy);
        }

        NaviagationHelper.GoBack();
      }
    }

    private bool ValidateAlbumName(string name)
    {
      const int maxNameLength = 128;
      if (string.IsNullOrEmpty(name))
      {
        MessageBox.Show(AppResources.Enter_album_name_UC);
        return false;
      }

      if (name.Length > maxNameLength)
      {
        MessageBox.Show(string.Format(AppResources.Length_of_name_should_be_less_then_UC, maxNameLength));
        return false;
      }

      return true;
    }

    private string GetPrivacyByAccessProperties()
    {
      if (AllUsersAccess)
      {
        return "0";
      }
      else if (FriendsAccess)
      {
        return "1";
      }
      else if (FriendsAdnFriendsOfFriendsAccess)
      {
        return "2";
      }

      return "3";
    }

    private void SetAccessPropertiesByPrivacy(VKPrivacy privacy)
    {
      if (privacy != null && privacy.type != null)
      {
        switch (privacy.type)
        {
          case "all":
            AllUsersAccess = true;
            break;
          case "friends":
            FriendsAccess = true;
            break;
          case "friends_of_friends":
            FriendsAdnFriendsOfFriendsAccess = true;
            break;
          default:
            OnlyMeAccess = true;
            break;
        }
      }
      else
      {
        OnlyMeAccess = true;
      }
    }

    #endregion
  }
}
