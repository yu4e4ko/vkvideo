﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using VkVideo.VkModels;

namespace VkVideo.VkRepositories.Cache
{
  [DataContract]
  [KnownType(typeof(List<VkCommentWithProfile>))]
  public sealed class CommentsVkRepositoryCache<T> : IVkRepositoryCache<List<VkCommentWithProfile>>
  {
    [DataMember]
    private List<VkCommentWithProfile> _cachedVideoCommentsForVideo;
    private long _cachedCommentsForVideoId;

    public CommentsVkRepositoryCache()
    {
      _cachedVideoCommentsForVideo = new List<VkCommentWithProfile>();
    }

    public void AddInCache(List<VkCommentWithProfile> item, object additionalData = null)
    {
      _cachedVideoCommentsForVideo = item;

      var video = additionalData as VkVideoWithFile;
      if (video != null)
      {
        _cachedCommentsForVideoId = video.id;
      }
      else
      {
        ResetCache();
      }
    }

    public List<VkCommentWithProfile> GetFromCahce(object additionalData = null)
    {
      if (_cachedVideoCommentsForVideo == null)
      {
        throw new Exception("Comments cache is NULL");
      }

      return _cachedVideoCommentsForVideo;
    }

    public bool IsCached(object additionalData = null)
    {
      if (additionalData != null && additionalData as VkVideoWithFile != null)
      {
        return _cachedVideoCommentsForVideo != null && _cachedCommentsForVideoId == (additionalData as VkVideoWithFile).id;
      }

      return false;
    }

    public void ResetCache(object additionalData = null)
    {
      _cachedCommentsForVideoId = -1;
      _cachedVideoCommentsForVideo = null;
    }
  }
}
