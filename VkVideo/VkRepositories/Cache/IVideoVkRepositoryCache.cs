﻿using System.Collections.Generic;
using VkVideo.VkModels;

namespace VkVideo.VkRepositories.Cache
{
  public interface IVideoVkRepositoryCache : IVkRepositoryCache<List<VkVideoWithFile>>
  {
    void AddVideoInCahce(VkVideoWithFile video, string albumId);

    void RemoveVideoFromCache(VkVideoWithFile video, string albumId);
  }
}
