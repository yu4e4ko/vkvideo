﻿//using Moq;
//using NUnit.Framework;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using VK.WindowsPhone.SDK.API;
//using VK.WindowsPhone.SDK.API.Model;
//using VkVideo.VkModels;
//using VkVideo.VkRepositories;

//namespace VkVideoTests.VkRepositoryTests
//{
//  [TestFixture]
//  public class VideoTests
//  {
//    private const int DefaultVideoListCount = 100;
//    private List<VkVideoWithFile> _videoList;
//    private VKBackendResult<int> _intBackendResult;
//    private VKBackendResult<VKList<int>> _listIntBackendResult;

//    private Mock<IVkApiProvider> _mock;
//    private VkRepository _vkRepository;

//    [OneTimeSetUp]
//    public void Init()
//    {
//      _videoList = new List<VkVideoWithFile>();
      
//      for(var i = 0; i < DefaultVideoListCount; i++)
//      {
//        _videoList.Add(new VkVideoWithFile());
//      }

//      _intBackendResult = new VKBackendResult<int>
//      {
//        ResultCode = VKResultCode.Succeeded
//      };

//      _listIntBackendResult = new VKBackendResult<VKList<int>>
//      {
//        ResultCode = VKResultCode.Succeeded
//      };
//    }

//    [SetUp]
//    public void SetUp()
//    {
//      _mock = new Mock<IVkApiProvider>();
//      _mock.Setup(r => r.VideoGet(null)).ReturnsAsync(_videoList);
//      _mock.Setup(r => r.LoadUser()).ReturnsAsync(new VKUser());

//      _mock.Setup(r => r.DeleteUserVideo(It.IsAny<VkVideoWithFile>()))
//        .ReturnsAsync(_intBackendResult);

//      _mock.Setup(r => r.DeleteVideoFromAlbum(It.IsAny<VkVideoWithFile>(), It.IsAny<string[]>()))
//        .ReturnsAsync(_listIntBackendResult);

//      _mock.Setup(r => r.GetAlbumsByVideo(It.IsAny<VkVideoWithFile>())).ReturnsAsync(new List<string>());

//      _vkRepository = new VkRepository(_mock.Object);
//    }

//    [Test]
//    public async Task VideoTests_ShouldBeReturnDefaultCount()
//    {
//      // Arrange
//      var expectedCount = DefaultVideoListCount;

//      // Act
//      var video = await _vkRepository.GetUserVideoList();

//      // Assert
//      Assert.AreEqual(expectedCount, video.Count);
//    }

//    [Test]
//    public async Task VideoTests_ShouldBeInvokeLoadUserVideoOnce()
//    {
//      // Arrange
//      var expectedCount = DefaultVideoListCount;

//      // Act
//      var video = await _vkRepository.GetUserVideoList();
//      var userVideo = await _vkRepository.GetUserVideoList();
//      video = await _vkRepository.GetUserVideoList();

//      // Assert
//      _mock.Verify(r => r.VideoGet(null), Times.Once);
//    }

//    [Test]
//    public async Task VideoTests_ShouldBeVideoCacheResetAfterDeleteVideo()
//    {
//      // Arrange
//      var expectedInvokeCount = 2;

//      // Act
//      // first invoke
//      var video = await _vkRepository.GetUserVideoList();

//      // reset chace after delete
//      await _vkRepository.DeleteVideoFromAllAlbums(video[0]);
//      video = await _vkRepository.GetUserVideoList();

//      // Assert
//      _mock.Verify(r => r.VideoGet(null), Times.Exactly(expectedInvokeCount));
//    }
//  }
//}
