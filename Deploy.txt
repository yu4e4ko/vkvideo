Приложение разработано на технологии "Windows phone 8.1 SilverLight".
Язык программирования: C#.
.Net framework: 4.5
При разработке использовалась Visual Studio 2015 Professional.
Приложение тестировалось на эмуляторе "Emulator 8.1 WVGA 4 inch 512MB" и смартфоне "Nokia Lumia 630".

Solution "VkVideoManager" состоит из 3-ех проектов:
1) Vk.WindowsPhone.SDK (Windows Phone 8.0) - SDK Вконтакте
2) VkVideo (Windows Phone Silverlight 8.1) - разрабатываемое мобильное приложение (должно быть указано в солюшене как StartUp project)
3) VkVideoTests - проект с юнит тестами.

Приложение находится в магазине с доступностью BETA TEST для платформы Windows phone 8.1

Тестировщик со стороны Вконтаке: vkmobilechallenge@gmail.com
Ссылка на приложение в магазине: https://www.microsoft.com/en-us/store/apps/vk-video-manager/9nblggh4r33m

Данной информации достаточно, чтобы развернуть приложение.
Дополнительную информацию можно узнать у меня (разработчик).
Связаться со мной в ВК (https://vk.com/yu4_walker).

С Уважением,
Чупыркин Юрий