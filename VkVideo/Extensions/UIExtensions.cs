﻿using Microsoft.Phone.Controls;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using VK.WindowsPhone.SDK.API;
using VkVideo.ApplicationModels;
using VkVideo.Resources;
using VkVideo.ViewModels;
using VkVideo.VkModels;

namespace VkVideo.Extensions
{
  public static class UIExtensions
  {
    public static Task<VkCustomDialogModel> ShowCustomDialog(
      this BaseViewModel viewModel,
      CustomDialogOptionsModel options)
    {
      var messageBox = CreateCustomeMessageBox(options.Message, options.Title);

      var textBox = options.CaptchaUserRequest == null ? CreateTextBox(options) : new TextBox();

      if (options.CaptchaUserRequest != null)
      {
        messageBox.Content = CreateCaptchaGrid(options.CaptchaUserRequest, textBox);
        options.ContainsInput = false;
      }

      if (options.ContainsInput)
      {
        messageBox.Content = textBox;
      }

      var completion = new TaskCompletionSource<VkCustomDialogModel>();

      // Note: wire up the event that will be used as the result of this method
      EventHandler<DismissedEventArgs> dismissed = null;
      dismissed += (sender, args) =>
      {
        var model = new VkCustomDialogModel
        {
          CustomMessageBoxResult = args.Result,
          InputText = textBox.Text ?? string.Empty
        };

        completion.SetResult(model);

        // Note: make sure we unsubscribe from this!
        messageBox.Dismissed -= dismissed;
      };

      messageBox.Dismissed += dismissed;
      messageBox.Show();

      return completion.Task;
    }

    #region privates

    private static Grid CreateCaptchaGrid(VKCaptchaUserRequest captchaUserRequest, TextBox textBox)
    {
      var grid = new Grid();
      grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(240) });
      grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(80) });

      var image = new Image
      {
        Source = new BitmapImage(new Uri(captchaUserRequest.Url))
      };

      Grid.SetRow(image, 0);
      Grid.SetRow(textBox, 1);

      grid.Children.Add(image);
      grid.Children.Add(textBox);

      return grid;
    }

    private static TextBox CreateTextBox(CustomDialogOptionsModel options)
    {
      var textBox = new TextBox
      {
        TextWrapping = TextWrapping.Wrap,
        AcceptsReturn = true,
        MaxHeight = 250,
        MaxLength = options.InputTextMaxLength.GetValueOrDefault(300),
        Text = options.InputText ?? string.Empty,
        SelectionStart = string.IsNullOrEmpty(options.InputText) == false ? options.InputText.Length : 0
      };

      return textBox;
    }

    private static CustomMessageBox CreateCustomeMessageBox(string message, string title = null)
    {
      var messageBox = new CustomMessageBox();

      messageBox.RightButtonContent = AppResources.Cancel_UC;
      messageBox.LeftButtonContent = "OK";
      messageBox.Message = message ?? string.Empty;
      messageBox.Title = title ?? string.Empty;

      messageBox.Background = StyleResources.MainBackgroundBrush;
      messageBox.Foreground = StyleResources.MainForegroundBrush;

      return messageBox;
    }

    #endregion
  }
}
