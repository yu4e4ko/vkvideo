﻿namespace VkVideo.VkRepositories.Cache
{
  public interface IVkRepositoryCache<T> where T : new()
  {
    bool IsCached(object additionalData = null);

    void AddInCache(T item, object additionalData = null);

    T GetFromCahce(object additionalData = null);

    void ResetCache(object additionalData = null);
  }
}
