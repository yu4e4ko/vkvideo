﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VkVideo.VkRepositories;
using VkVideo.VkModels;
using System.Collections.ObjectModel;

namespace VkVideo.ViewModels
{
  // Note: all logic is replaced in partial classes
  public partial class VideoListViewModel : BaseViewModel
  {
    public VideoListViewModel()
    {
      _video = new List<VkVideoWithFile>();
      _albums = new List<VkAlbumWithPhoto>();
      _searchedVideo = new List<VkVideoWithFile>();
      _catalogRows = new ObservableCollection<VkCatalog>();
    }

    public async Task Init()
    {
      await InitVideo();
      await SearchAlbums();

      _catalogs = await VkRepositoryResolver.Instance.GetCatalogs();
      CatalogRows = new ObservableCollection<VkCatalog>(_catalogs.Items);

      MoreButtonEnabled = true;
      SetMoreCatalogsButtonVisibility();
    }
  }
}
