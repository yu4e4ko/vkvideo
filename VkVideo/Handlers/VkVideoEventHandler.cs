﻿using System.Windows;
using VkVideo.Services;

namespace VkVideo.Handlers
{
  public class VkVideoEventHandler
  {
    public static void HandleBackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      if (LoadingIconResolver.Instance.LoadingVisibility == Visibility.Visible)
      {
        e.Cancel = true;
      }
    }
  }
}
