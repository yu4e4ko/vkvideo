﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using VkVideo.VkRepositories;
using VkVideo.Extensions;
using VkVideo.VkModels;
using VkVideo.Resources;
using VkVideo.ApplicationModels;
using VkVideo.Helpers;

namespace VkVideo.ViewModels
{
  public class VideoViewModel : BaseViewModel
  {
    private VkVideoWithFile _video;
    private List<VkCommentWithProfile> _comments;
    private string _currentMp4Format;
    private bool? _canEdit;
    private Visibility _showRadiobuttons;

    public const string Mp4_240 = "240";
    public const string Mp4_360 = "360";
    public const string Mp4_480 = "480";
    public const string Mp4_720 = "720";

    public VideoViewModel()
    {
      _comments = new List<VkCommentWithProfile>();
    }

    #region properties

    public VkVideoWithFile Video
    {
      get
      {
        return _video;
      }
      set
      {
        _video = value;
        OnPropertyChanged("Video");
      }
    }

    public List<VkCommentWithProfile> Comments
    {
      get
      {
        return _comments;
      }
      set
      {
        _comments = value;
        OnPropertyChanged("Comments");
      }
    }

    public async Task<bool> CanEdit()
    {
      if (_canEdit == null)
      {
        var user = await VkRepositoryResolver.Instance.GetCurrentUser();
        _canEdit = _video.owner_id == user.id;
        //_canEdit = _video.owner_id == Task.Run(() => VkRepositoryResolver.Instance.GetCurrentUser()).Result.id;
      }

      return _canEdit.Value;
    }

    public Visibility ShowRadiobuttons
    {
      get
      {
        return _showRadiobuttons;
      }
      set
      {
        _showRadiobuttons = value;
        OnPropertyChanged("ShowRadiobuttons");
      }
    }

    #endregion

    #region methods

    public async Task Init()
    {
      Video = VkRepositoryResolver.Instance.CurrentVideo;

      // Note: catalogs return video without files
      if (Video.Files.GetVideoFormat() == VideoFormat.None)
      {
        Video = await ReloadVideoFiles(Video);
      }

      SetRadiobuttonsVisibility();
      await LoadComments();
    }

    public async Task<bool> CommentsAllowed()
    {
      var user = await VkRepositoryResolver.Instance.GetCurrentUser();
      return user.id == _video.owner_id || _video.can_comment;
    }

    public async Task LoadComments()
    {
      Comments = await VkRepositoryResolver.Instance.LoadVideoComments(Video);
    }

    public void ShowVideo()
    {
      switch (_video.Files.GetVideoFormat())
      {
        case VideoFormat.External:
          var webBrowserTask = new WebBrowserTask();
          webBrowserTask.Uri = new Uri(GetVideoUrl(), UriKind.Absolute);
          webBrowserTask.Show();
          break;
        case VideoFormat.Flv:
          MessageBox.Show(AppResources.Video_cannot_be_played_on_this_device_UC);
          break;
        case VideoFormat.Mp4:
          var mediaPlayerLauncher = new MediaPlayerLauncher();
          mediaPlayerLauncher.Media = new Uri(GetVideoUrl(), UriKind.Absolute);
          mediaPlayerLauncher.Show();
          break;
        default:
          throw new ArgumentException(AppResources.Video_format_is_invalid_UC);
      }
    }

    public void RadioButtonChecked(string value)
    {
      switch (value)
      {
        case VideoViewModel.Mp4_240:
          _currentMp4Format = VideoViewModel.Mp4_240;
          break;
        case VideoViewModel.Mp4_360:
          _currentMp4Format = VideoViewModel.Mp4_360;
          break;
        case VideoViewModel.Mp4_480:
          _currentMp4Format = VideoViewModel.Mp4_480;
          break;
        case VideoViewModel.Mp4_720:
          _currentMp4Format = VideoViewModel.Mp4_720;
          break;
        default:
          _currentMp4Format = null;
          break;
      }
    }

    public async Task<bool> AddComment()
    {
      var dialogOptions = new CustomDialogOptionsModel
      {
        Message = AppResources.Add_comment_UC,
        ContainsInput = true
      };

      var customMessageBox = await this.ShowCustomDialog(dialogOptions);
      if (customMessageBox.CustomMessageBoxResult == CustomMessageBoxResult.LeftButton)
      {
        var comment = customMessageBox.InputText;
        if (string.IsNullOrEmpty(comment) == false)
        {
          await VkRepositoryResolver.Instance.CreateComment(_video, comment);
          await LoadComments();

          return true;
        }
      }

      return false;
    }

    public async Task EditTitle()
    {
      await EditVideo(true);
    }

    public async Task EditDescription()
    {
      await EditVideo();
    }

    private async Task EditVideo(bool editTitle = false)
    {
      var dialogOptions = new CustomDialogOptionsModel
      {
        Message = editTitle ? AppResources.Edit_title_UC : AppResources.Edit_description_UC,
        ContainsInput = true,
        InputText = editTitle ? _video.title : _video.description,
        InputTextMaxLength = editTitle ? 60 : 500
      };

      var customMessageBox = await this.ShowCustomDialog(dialogOptions);

      if (customMessageBox.CustomMessageBoxResult == CustomMessageBoxResult.LeftButton)
      {
        var resultString = customMessageBox.InputText.Trim();
        if (string.IsNullOrEmpty(resultString))
        {
          return;
        }

        var title = editTitle ? resultString : _video.title;
        var description = editTitle ? _video.description : resultString;

        var response = await VkRepositoryResolver.Instance.EditVideo(_video, title, description);

        if (response == 1)
        {
          Video.title = title;
          Video.description = description;
          Video = await ReloadVideoFiles(Video);
        }
        else
        {
          CommonHelper.ShowError("Edit video error. Error code: " + response);
        }
      }
    }

    private string GetVideoUrl()
    {
      if (_video.Files.GetVideoFormat() == VideoFormat.Flv)
      {
        return _video.Files.Flv_320;
      }
      else if (_video.Files.GetVideoFormat() == VideoFormat.External)
      {
        return _video.Files.External;
      }
      else
      {
        if (string.IsNullOrEmpty(_currentMp4Format))
        {
          throw new ArgumentNullException(AppResources.Video_format_is_null_UC);
        }

        switch (_currentMp4Format)
        {
          case VideoViewModel.Mp4_240:
            return _video.Files.Mp4_240;
          case VideoViewModel.Mp4_360:
            return _video.Files.Mp4_360;
          case VideoViewModel.Mp4_480:
            return _video.Files.Mp4_480;
          case VideoViewModel.Mp4_720:
            return _video.Files.Mp4_720;
          default:
            throw new ArgumentNullException(AppResources.Video_format_is_invalid_UC);
        }
      }
    }

    private async Task<VkVideoWithFile> ReloadVideoFiles(VkVideoWithFile video)
    {
      var reloadedVideo = await VkRepositoryResolver.Instance.GetVideoById(
        video.id.ToString(), video.owner_id.ToString());

      return reloadedVideo;
    }

    private void SetRadiobuttonsVisibility()
    {
      ShowRadiobuttons = Video.ExternalFormat ? Visibility.Collapsed : Visibility.Visible;
    }

    #endregion
  }
}
