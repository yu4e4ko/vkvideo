﻿using System;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using VK.WindowsPhone.SDK;
using VkVideo.VkRepositories;
using VkVideo.Helpers;
using System.Threading;
using System.Globalization;
using VkVideo.ApplicationContext;

namespace VkVideo.ViewModels
{
  public class LoginViewModel : BaseViewModel
  {
    private readonly EventHandler _switchLanguageHandler;

    private BitmapImage _photo;
    private bool _isLoaded;
    private bool _englishLang;
    private bool _russianLang;

    public LoginViewModel(EventHandler switchLanguageHandler)
    {
      _switchLanguageHandler = switchLanguageHandler;

      VKSDK.WakeUpSession();
      RussianLang = true;
    }

    #region properties

    public bool IsLoaded
    {
      get
      {
        return _isLoaded;
      }
      set
      {
        _isLoaded = value;
        OnPropertyChanged("IsLoaded");
      }
    }

    public BitmapImage Photo
    {
      get
      {
        return _photo;
      }
      set
      {
        _photo = value;
        OnPropertyChanged("Photo");
      }
    }

    public bool EnglishLang
    {
      get
      {
        return _englishLang;
      }
      set
      {
        _englishLang = value;

        if (_englishLang)
        {
          ChangeLanguage("en-US");
        }

        OnPropertyChanged("EnglishLang");
      }
    }

    public bool RussianLang
    {
      get
      {
        return _russianLang;
      }
      set
      {
        _russianLang = value;

        if (_russianLang)
        {
          ChangeLanguage("ru-RU");
        }

        OnPropertyChanged("RussianLang");
      }
    }

    #endregion

    #region methods

    public async Task Init()
    {
      await CheckSession();
    }

    public async Task Manage()
    {
      // Note: load in cache
      await VkRepositoryResolver.Instance.GetCatalogs();
      NaviagationHelper.Navigate("/Views/VideoListView.xaml");
    }

    public async Task LogOut()
    {
      VKSDK.Logout();
      await CheckSession();
    }

    private void ChangeLanguage(string lang)
    {
      if (string.IsNullOrEmpty(lang) && lang != "en-US" && lang != "ru-RU")
      {
        return;
      }

      var culture = new CultureInfo(lang);
      Thread.CurrentThread.CurrentCulture = culture;
      Thread.CurrentThread.CurrentUICulture = culture;

      if (_switchLanguageHandler != null)
      {
        _switchLanguageHandler(null, null);
      }
    }

    #region privates

    private async Task CheckSession()
    {
      bool isLoggedIn = VKSDK.IsLoggedIn;

      if (isLoggedIn)
      {
        var user = await VkRepositoryResolver.Instance.GetCurrentUser();
        Photo = new BitmapImage(new Uri(user.photo_200, UriKind.Absolute));

        // Note: load in cache
        await VkRepositoryResolver.Instance.GetAlbumVideoList(ApplicationConstants.MyVideosAlbumId);

        IsLoaded = true;
      }
      else
      {
        NaviagationHelper.Navigate("/MainPage.xaml");
      }
    }

    #endregion

    #endregion
  }
}
