﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using VkVideo.VkModels;

namespace VkVideo.VkRepositories.Cache
{
  [DataContract]
  [KnownType(typeof(List<VkVideoWithFile>))]
  public sealed class SearchVideoVkRepositoryCache<T> : IVkRepositoryCache<List<VkVideoWithFile>>
  {
    [DataMember]
    private List<VkVideoWithFile> _cachedSearchedVideo;
    private string _lastSearchedText;

    public void AddInCache(List<VkVideoWithFile> item, object additionalData = null)
    {
      var searchText = additionalData as string;

      _lastSearchedText = searchText;
      _cachedSearchedVideo = item;
    }

    public List<VkVideoWithFile> GetFromCahce(object additionalData = null)
    {
      if (_cachedSearchedVideo == null)
      {
        throw new Exception("SearchVideo cache is NULL");
      }

      return _cachedSearchedVideo;
    }

    public bool IsCached(object additionalData = null)
    {
      var searchText = additionalData as string;

      return _lastSearchedText != null && _lastSearchedText == searchText;
    }

    public void ResetCache(object additionalData = null)
    {
      _cachedSearchedVideo = null;
      _lastSearchedText = null;
    }
  }
}
