﻿using System;
using System.Collections.Generic;
using System.Windows;
using Microsoft.Phone.Controls;
using VK.WindowsPhone.SDK;
using VkVideo.Helpers;
using VkVideo.VkRepositories;

namespace VkVideo
{
  public partial class MainPage : PhoneApplicationPage
  {
    private const string VkApplicationID = "5202167";
    private readonly List<String> _scope = new List<string> { VKScope.VIDEO };

    public MainPage()
    {
      InitializeComponent();
    }

    #region events

    private void MainPage_Loaded(object sender, RoutedEventArgs e)
    {
      VKSDK.Initialize(VkApplicationID);

      VKSDK.AccessTokenReceived += (sender1, args) =>
      {
        UpdateUIState();
      };

      VKSDK.WakeUpSession();
      VKSDK.CaptchaRequest = CommonHelper.CaptchaRequest;

      UpdateUIState();
    }

    private void BtnLogin_Click(object sender, RoutedEventArgs e)
    {
      Authorize(LoginType.WebView);
    }

    private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      e.Cancel = true;
      App.Current.Terminate();
    }

    #endregion

    #region privates

    private void Authorize(LoginType loginType)
    {
      var errorMessage = "Erorr:\n";

      try
      {
        VKSDK.Authorize(_scope, false, false);
      }
      catch (AggregateException ex)
      {
        foreach (var innerEx in ex.InnerExceptions)
        {
          errorMessage += innerEx.Message + ";\n";
        }

        MessageBox.Show(errorMessage);
      }
      catch (Exception ex)
      {
        errorMessage += ex.Message;
        MessageBox.Show(errorMessage);
      }
    }

    private void UpdateUIState()
    {
      bool isLoggedIn = VKSDK.IsLoggedIn;

      if (isLoggedIn)
      {
        VkRepositoryResolver.Instance.ResetCash();
        NaviagationHelper.Navigate("/Views/LoginView.xaml");
      }
    }

    #endregion
  }
}